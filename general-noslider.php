<?php
/*
Template Name: No Slider
*/
?>
        	
<?php get_header(); ?>

    <body id="default" <?php body_class($page_slug); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        
        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<?php include('includes/menuBoards.php'); ?>

<!--

<div id="aboutYourStay">About Your Stay</div>

<div id="stayMap">
	<ul id="stayMap_Main">
		<li class="deluxerooms"><a href="#">Deluxe Rooms</a></li>
		<li class="superiorrooms"><a href="#">Superior Rooms</a></li>
		<li class="suites"><a href="#">Suites</a></li>
		<li class="bbrooms"><a href="#">B&B Rooms</a></li>
	</ul>
	<ul id="stayMap_Guest">
		<li class="firehouse"><a href="#">The Firehouse</a></li>
		<li class="mapleglen"><a href="#">Maple Glen</a></li>
		<li class="maple2"><a href="#">2 Maple Street</a></li>
		<li class="mcgregor"><a href="#">McGregor House</a></li>
		<li class="obrien"><a href="#">O'Brien House</a></li>
		<li class="cottage"><a href="#">The Cottage</a></li>
		<li class="stevens"><a href="#">Stevens House</a></li>
		<li class="stafford"><a href="#">Stafford House</a></li>
		<li class="meadowlark"><a href="#">Meadowlark</a></li>
	</ul>
</div>
-->

<div id="interiorMiddleWrap">
<div id="interiorMiddle">

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<h1><?php the_title(); ?></h1>
<?php the_content(); ?>
<?php endwhile; endif; ?>

</div><!-- /interiorMiddle -->
</div>

</div><!-- /interiorWrap-->

<?php get_footer(); ?>