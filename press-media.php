<?php
/*
Template Name: Press & Media
*/
?>

<?php get_header(); ?>

    <body id="default" <?php body_class('press'); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<div id="interiorMiddleWrap">

<img src="<?php bloginfo('stylesheet_directory'); ?>/images/info/pressHdr.png" class="pressHdr" />

<div id="interiorMiddle">

<?php if (is_page('press-releases')) { ?>

<h1>press releases</h1>

<?php
$args = array(
    'posts_per_page' => 50,
    'post_type' => 'press-release'
);
query_posts($args);
if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <div class="pressRelease"><a href="<?php the_field('pdf'); ?>"><?php the_title(); ?></a></div>
<?php endwhile; endif; ?>

<?php wp_reset_query(); ?>

<?php } else { ?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<h1 class="balance-text"><?php the_title(); ?></h1>
<?php the_content(); ?>
<?php endwhile; endif; ?>

<?php } ?>

</div><!-- /interiorMiddle -->
</div>

<div id="interiorLeft">

<p><a href="<?php bloginfo('siteurl'); ?>/press-media-inquiries/photo-gallery/">IMAGE GALLERY</a></p>
<p><a href="<?php bloginfo('siteurl'); ?>/press-media-inquiries/recent-press/">RECENT PRESS</a></p>
<p><a href="<?php bloginfo('siteurl'); ?>/press-media-inquiries/video-clips/">VIDEO CLIPS</a></p>
<p><a href="<?php bloginfo('siteurl'); ?>/press-media-inquiries/">PRESS CONTACT</a></p>
<p><a href="<?php bloginfo('siteurl'); ?>/press-media-inquiries/social-media-presence/">SOCIAL MEDIA PRESENCE</a></p>
</div>

<div id="interiorRight">
<?php if (!is_page('press-releases')) { ?>

<h2>Current Press Releases <span>(pdf's)</span></h2>
<p>Click to download/view pdf files of<br/>our most current press releases.</p>

<?php
$args = array(
	'posts_per_page' => 10,
	'post_type' => 'press-release'
);
query_posts($args);
if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
	<div class="pressRelease"><a href="<?php the_field('pdf'); ?>"><?php the_title(); ?></a></div>
<?php endwhile; endif; ?>

<?php wp_reset_query(); ?>

<a href="<?php bloginfo('siteurl'); ?>/info/press-releases/" class="moreLink">More</a>

<?php } ?>

</div>

<?php include('includes/menuBoards.php'); ?>

</div><!-- /interiorWrap-->

<?php get_footer(); ?>
