<?php
/*
Template Name: Meet (Landing)
*/
?>
        	
<?php get_header(); ?>

    <body id="meet-landing">
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        
        <div id="topWrap">
        	<div id="topWrapBorder">
        	</div>
        </div>
        
          	
     	<div id="bottomWrap">
     		<div id="bottomWrapBorder">
     		</div>
     	</div>   
        
        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>



<div id="middleWindow">

<!-- BODY COPY GOES HERE -->

<ul id="meetNav">
<li><a class="meetings" href="<?php bloginfo('siteurl'); ?>/meetings-conferences/meeting-spaces/"><img src="<?php bloginfo('stylesheet_directory');?>/images/meetings-conferences/meetingsatRLI.png" alt="Meetings at the Red Lion Inn"></a></li>
<li><a class="gatherings" href="<?php bloginfo('siteurl'); ?>/meetings-conferences/family-accommodations/"><img src="<?php bloginfo('stylesheet_directory');?>/images/meetings-conferences/gatherings.png" alt="Family Gatherings & Celebrations"></a></li>
<li><a class="tours" href="<?php bloginfo('siteurl'); ?>/meetings-conferences/berkshire-group-tours/"><img src="<?php bloginfo('stylesheet_directory');?>/images/meetings-conferences/groupstravel.png" alt="Group Tours & Travel"></a></li>
<li><a class="weddings" href="<?php bloginfo('siteurl'); ?>/meetings-conferences/weddings/"><img src="<?php bloginfo('stylesheet_directory');?>/images/meetings-conferences/weddingsatRLI.png" alt="Weddings at the Red Lion Inn"></a></li>
</ul>

</div>


<?php get_footer(); ?>

