<div id="below">
      <?php wp_reset_query(); ?>
      <?php if ( is_front_page() ) { 	?>
        		<ul id="bottomNav">
        			<li class="nav1_btm"><a href="<?php bloginfo('siteurl'); ?>/our-story/history-about-the-inn/">Our Story</a></li>
        			<li class="nav2_btm"><a href="<?php bloginfo('siteurl'); ?>/sustainability-community/">Sustainability & Community</a></li>
        			<li class="nav3_btm"><a href="<?php bloginfo('siteurl'); ?>/things-to-do-in-the-berkshires/">The Beautiful Berkshires</a></li>
        			<li class="nav4_btm"><a href="<?php bloginfo('siteurl'); ?>/amenities-activities/">Amenities & Activities</a></li>
        			<li class="nav5_btm"><a href="<?php bloginfo('siteurl'); ?>/press-media-inquiries/">Press & Media</a></li>
        			<li class="nav6_btm"><a href="<?php bloginfo('siteurl'); ?>/employment-opportunities/">Employment & Opportunities</a></li>
        		</ul>
  <?php } ?>


    <?php if ( is_home()) { ?>
		<div id="breadcrumbs">
        		<div>Where am I?</div>
        		<div xmlns:v="http://rdf.data-vocabulary.org/#" id="crumbs"><span typeof="v:Breadcrumb"><a href="http://www.redlioninn.com/" property="v:title" rel="v:url">Home</a></span> » <span class="current">Blog</span></div>
        		</div>
	<?php } else if ( !is_front_page() || !is_home()) { ?>
		<div id="breadcrumbs">
        		<div>Where am I?</div>
	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
		</div>
	<?php } ?>



        	<div id="footerBar">
				<div class="ftrLeft">
					30 Main Street&nbsp;&nbsp;Stockbridge, MA 01262
				</div>
				<div class="ftrRight">
					<a class="ftrEmail" href="mailto:info@redlioninn.com">EMAIL US</a>
					&nbsp; &nbsp; Tel (413) 298-5545
				</div>
        	</div><!-- /footerBar -->


        	<div id="social">
        		<ul>
        			<li><a class="fb" href="http://www.facebook.com/TheRedLionInnBerkshiresMA">Facebook</a></li>
        			<li><a class="twitter" href="http://twitter.com/TheRedLionInn">Twitter</a></li>
        			<li><a class="tripadvisor" href="http://www.tripadvisor.com/Hotel_Review-g41850-d89873-Reviews-The_Red_Lion_Inn-Stockbridge_Massachusetts.html">TripAdvisor</a></li>
        			<li><a class="blog" href="<?php bloginfo('url'); ?>/blog">Blog</a></li>
        			<li><a class="instagram" href="http://www.instagram.com/redlioninn">Instagram</a></li>
				</ul>
        	</div><!-- /social -->


        	<div id="emailSubscribe">

        	<!-- BEGIN: Constant Contact Basic Opt-in Email List Form -->
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:2;">
<input type="hidden" name="llr" value="9htafjcab">
<input type="hidden" name="m" value="1101966363898">
<input type="hidden" name="p" value="oi">
<input type="text" name="ea" size="20" value="" class="emailAddress">
<input type="submit" name="go" value="submit" class="submit" onClick="_gaq.push(['_trackEvent', 'Email Signup', 'Submit']);">
</form>
<!-- END: Constant Contact Basic Opt-in Email List Form -->
<!-- BEGIN: SafeSubscribe
<div align="center" style="padding-top:5px;">
<img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt=""/>
</div>

<div align="center" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;">
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank" rel="nofollow">Email Marketing</a> you can trust
</div>
END: Email Marketing you can trust -->


        	</div><!-- /emailSubscribe -->

			<div id="directions">
				<a href="<?php bloginfo('url'); ?>/property-map/"></a>
			</div>

			<div id="weather"></div>


              <?php if ( !is_front_page() || is_page_template('offers.php')) { 	?>
					<ul id="bottomLinks">
        			<li><a href="<?php bloginfo('siteurl'); ?>/our-story/history-about-the-inn">Our Story</a></li>
        			<li><a href="<?php bloginfo('siteurl'); ?>/sustainability-community">Sustainability & Community</a></li>
        			<li><a href="<?php bloginfo('siteurl'); ?>/things-to-do-in-the-berkshires">The Beautiful Berkshires</a></li>
        			<li><a href="<?php bloginfo('siteurl'); ?>/amenities-activities">Amenities & Activities</a></li>
        			<li><a href="<?php bloginfo('siteurl'); ?>/press">Press & Media</a></li>
        			<li><a href="<?php bloginfo('siteurl'); ?>/employment-opportunities">Employment & Opportunities</a></li>
					</ul>
        <?php } ?>

		<a href="http://www.historichotels.org/" id="hha"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/hha-logo.png"></a>
        <a href="http://www.mainstreethospitalitygroup.com" id="mshg"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/MainStreetHospitality-200.png"></a>
        <br/><a class="hcCredit" href="http://www.housatoniccreative.com">website by Housatonic Creative</a>


        	</div><!-- /below -->
	    </div><!-- /mainWrap -->

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-2192313-2', 'redlioninn.com');
          ga('require', 'displayfeatures');
          ga('send', 'pageview');

        </script>
        <?php wp_footer() ?>
    </body>
</html>
