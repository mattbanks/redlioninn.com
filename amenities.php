<?php
/*
Template Name: Amenities & Activities
*/
?>

<?php get_header(); ?>

    <body id="default" <?php body_class('amenities'); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<?php include ('includes/sliders-headers.php'); ?>

<div id="interiorMiddleWrap">
<div id="interiorMiddle">

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<h1 class="balance-text"><?php the_title(); ?></h1>
<?php the_content(); ?>
<?php endwhile; endif; ?>

</div><!-- /interiorMiddle -->
</div>

<div id="interiorLeft">
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/fitness-room">Fitness room</a></p>
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/massage-services">Massage</a></p>
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/partner-spa">Partner Spa</a></p>
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/in-room-salon-service">In-room salon service</a></p>
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/year-round-pool-hot-tub">Year-round pool <span class="amp">&</span> hot tub</a></p>
<p>&bull; &bull; &bull;</p>
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/complimentary-wifi">Complimentary wifi</a></p>
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/movies">Movies</a></p>
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/turn-down-service">Turn Down Service</a></p>
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/room-service">Room Service</a></p>
<p><a href="<?php bloginfo('url'); ?>/berkshire-accommodations/enhance-your-stay/in-room-gifts/">In-Room Gifts</a></p>
<p>&bull; &bull; &bull;</p>
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/free-parking">Free Parking</a></p>
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/grocery-and-lunch-delivery">Grocery and lunch delivery</a></p>
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/quiet-libraryreading-room">Quiet Library/Reading Room</a></p>
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/front-porch">Front Porch</a></p>
<p><a href="<?php bloginfo('url'); ?>/amenities-activities/lobby-cat">Lobby Cat</a></p>
</div>

<div id="interiorRight">
<p><h3>Fun</h3>
<a href="<?php bloginfo('url'); ?>/amenities-activities/lions-den-entertainment" class="id1719">Lion’s Den Entertainment</a>
<a href="<?php bloginfo('url'); ?>/amenities-activities/the-pool-house" class="id2100">The Pool House</a>
<a href="<?php bloginfo('url'); ?>/amenities-activities/play-chess" class="id2102">Play Chess</a>
</p>

<p><h3>History</h3>
<a href="<?php bloginfo('url'); ?>/amenities-activities/take-a-self-guided-tour" class="id1723">Take a Self-Guided Tour</a>
<a href="<?php bloginfo('url'); ?>/amenities-activities/the-historic-guest-register" class="id1723">The Historic Guest Register</a>
</p>

<p><h3>Explore</h3>
<a href="<?php bloginfo('url'); ?>/amenities-activities/art-rli" class="id2459">Art @ RLI</a>
</p>

<p><h3>Food <span class="amp">&</span> Wine</h3>
<a href="<?php bloginfo('url'); ?>/amenities-activities/wine-tastings-and-dinners" class="id1727">Wine Tastings & dinners</a></p>

<p><h3>Children</h3>
<a href="<?php bloginfo('url'); ?>/amenities-activities/the-pool-house" class="id2100">The Pool & Pool House</a>
<a href="<?php bloginfo('url'); ?>/amenities-activities/movies" class="id1806">Movies</a>
<a href="<?php bloginfo('url'); ?>/amenities-activities/games" class="id2112">Games</a>
<a href="<?php bloginfo('url'); ?>/amenities-activities/babysitters" class="id2115">Babysitters</a>
</p>

<p><h3>Intellectual</h3>
<a href="<?php bloginfo('url'); ?>/amenities-activities/book-signings" class="id1733">Book signings</a>
<a href="<?php bloginfo('url'); ?>/amenities-activities/artist-reception" class="id1735">Artist reception</a>
</p>

<p><h3>Sports</h3>
<a href="<?php bloginfo('url'); ?>/amenities-activities/bicycles" class="id1737">Cycling</a>
<a href="<?php bloginfo('url'); ?>/amenities-activities/hikes" class="id1739">Hikes</a>
<a href="<?php bloginfo('url'); ?>/amenities-activities/golf" class="id1743">Golf</a>
<a href="<?php bloginfo('url'); ?>/amenities-activities/tennis" class="id1745">Tennis</a>
<a href="<?php bloginfo('url'); ?>/amenities-activities/yoga" class="id2118">Yoga</a>
</p>

</div>

<?php include('includes/menuBoards.php'); ?>

</div><!-- /interiorWrap-->

<?php get_footer(); ?>
