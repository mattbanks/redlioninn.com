<?php
/*
Template Name: No Slider
*/
?>

<?php get_header(); ?>

    <body id="default" <?php body_class($page_slug); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<?php include ('includes/sliders-headers.php'); ?>

<?php include('includes/menuBoards.php'); ?>

<div id="interiorMiddleWrap">
<div id="interiorMiddle">

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<h1 class="balance-text"><?php the_title(); ?></h1>
<?php the_content(); ?>
<?php endwhile; endif; ?>

</div><!-- /interiorMiddle -->
</div>

<?php if (is_tree('71')) { ?>
<div id="interiorLeft">
	<ul class="tertiaryNav">
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-accommodations/enhance-your-stay/red-lion-inn-guest-services/">Guest Services</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-accommodations/enhance-your-stay/in-room-gifts/">In-Room Gifts</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-accommodations/enhance-your-stay/massage-at-the-red-lion-inn/">Massage</a></li>
		</ul>
</div>
<?php } else if (is_tree('444')) { ?>
<div id="interiorLeft">
	<ul class="tertiaryNav">
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-accommodations/practical-guest-information/check-in">Check In</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-accommodations/practical-guest-information/reservation-policies">Reservation Policies</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-accommodations/practical-guest-information/cell-service">Cell Service</a></li>
		</ul>
</div>

<?php } ?>


</div><!-- /interiorWrap-->

<?php get_footer(); ?>
