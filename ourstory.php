<?php
/*
Template Name: Our Story
*/
?>

<?php get_header(); ?>

    <body id="default" <?php body_class('page-ourstory'); ?>>
      <a name="top"></a>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<?php include('includes/menuBoards.php'); ?>

<?php
		$slug = the_slug();
		$args = array(
			'posts_per_page' => 10,
			'post_type' => 'slide',
			'slide-page' => 'our-story'
		);
		query_posts($args);
		if ( have_posts() ) : ?>

		<div class="flexslider">
  			<ul class="slides">

		<?php while ( have_posts() ) : the_post(); ?>

    <li>
    	<img src="<?php the_field('image'); ?>" alt="<?php the_title(); ?>">
    	<div class="slideDescription"><?php the_field('description'); ?></div>
    </li>

<?php endwhile; ?>

  </ul></div>

<?php endif; ?>
<?php wp_reset_query(); ?>


<a name="content"></a><!-- ANCHOR TAG TO JUMP TO CONTENT -->

<div id="interiorMiddleWrap">

<div id="interiorMiddle">

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<?php if (is_singular('team')) { ?>

<div class="teamMemberSingle">
	<h1 class="balance-text"><?php the_title(); ?></h1>
	<h2 class="balance-text"><?php the_field('job_title'); ?></h2>
	<?php the_post_thumbnail(); ?>
	<?php the_content(); ?>
	<div class="contact">Contact <?php the_title(); ?></div>
	<a class="email" href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?>
</div>

<?php } else { ?>

	<h1 class="balance-text"><?php the_title(); ?></h1>
	<?php the_content(); ?>
	<img class="center" src="<?php bloginfo('stylesheet_directory'); ?>/images/lion-lg.jpg" />

<?php } endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>



</div><!-- /interiorMiddle -->

<div id="interiorLeft">
<img src="<?php bloginfo('stylesheet_directory'); ?>/images/ourStory.png">

	<ul class="navOurStory left">
		<li class="id<?php the_ID(); ?> pg268"><a href="<?php bloginfo('siteurl'); ?>/our-story/history-about-the-inn/#content">HISTORY OF THE INN</a></li>
		<li class="id<?php the_ID(); ?> pg275"><a href="<?php bloginfo('siteurl'); ?>/our-story/timeline-historyowner/#content">Historical Timeline</a></li>
		<li class="id<?php the_ID(); ?> pg301"><a href="<?php bloginfo('siteurl'); ?>/our-story/simon/#content">SIMON, Lobby Ambassador</a></li>
		<li class="id<?php the_ID(); ?> pg305"><a href="<?php bloginfo('siteurl'); ?>/our-story/the-lions-tales/#content">THE LION’S TALES <span>(Bedtime Stories)</span></a></li>
		<li class="id<?php the_ID(); ?> pg966"><a href="<?php bloginfo('siteurl'); ?>/our-story/our-sister-properties/#content">Sister Properties</a></li>
		<li class="id<?php the_ID(); ?> pg347"><a href="<?php bloginfo('siteurl'); ?>/our-story/mission-statement/#content">MISSION STATEMENT</a></li>
		<li class="id<?php the_ID(); ?> pg1397"><a href="<?php bloginfo('siteurl'); ?>/our-story/news-from-stockbridge/#content">NEWS FROM STOCKBRIDGE</a></li>
		<li class="id<?php the_ID(); ?> pg5325"><a href="<?php bloginfo('siteurl'); ?>/our-story/main-street-hospitality-group/#content">MAIN STREET HOSPITALITY GROUP</a></li>
	</ul>

<?php if(is_tree(268)) { ?>

	<div class="historyNavHdr">Choose a period in the Inn's history:</div>

	<ul class="tertiaryNav historyNav">
		<li><a href="<?php bloginfo('siteurl');?>/our-story/history-about-the-inn/revolutionary-era-beginnings/">Revolutionary Era <span>Beginnings</span></a></li>
		<li><a href="<?php bloginfo('siteurl');?>/our-story/history-about-the-inn/early-19th-century-a-transitional-period/">Early 19th Century: <span>A Transitional Period</span></a></li>
		<li><a href="<?php bloginfo('siteurl');?>/our-story/history-about-the-inn/late-19th-century-growth-in-the-gilded-age/">Late 19th Century: <span>Growth in the Gilded Age</span></a></li>
		<li><a href="<?php bloginfo('siteurl');?>/our-story/history-about-the-inn/the-turn-of-the-20th-century-one-familys-legacy/">The Turn of the 20th Century: <span>One Family’s Legacy</span></a></li>
		<li><a href="<?php bloginfo('siteurl');?>/our-story/history-about-the-inn/mid-20th-century-a-transitional-period/">Mid-20th Century: <span>A Transitional Period</span></a></li>
		<li><a href="<?php bloginfo('siteurl');?>/our-story/history-about-the-inn/late-20th-century-rescued-by-the-fitzpatricks/">Late 20th Century: <span>Rescued by the Fitzpatricks</span></a></li>
		<li><a href="<?php bloginfo('siteurl');?>/our-story/history-about-the-inn/the-21st-century-history-and-modernity-in-harmony/">The 21st Century: <span>History and Modernity in Harmony</span></a></li>
	</ul>
<?php } ?>

</div>


<div id="interiorRight">
	<ul class="navOurStory">
	</ul>

<!--
<div class="teamMember">
	<a href="http://www.redlioninn.com/team/nancy-fitzpatrick/">Nancy Fitzpatrick</a>
	<div class="jobTitle balance-text" style="white-space: normal; float: none; display: block; position: static;">Owner and Chairman</div>
</div>
<div class="teamMember">
	<a href="http://www.redlioninn.com/team/sarah-eustis/">Sarah Eustis</a>
	<div class="jobTitle balance-text" style="white-space: normal; float: none; display: block; position: static;">Chief Executive Officer</div>
</div>
<div class="teamMember">
	<a href="http://www.redlioninn.com/team/bruce-finn/">Bruce Finn</a>
	<div class="jobTitle balance-text" style="white-space: normal; float: none; display: block; position: static;">Chief Operating Officer</div>
</div>
<div class="teamMember">
	<a href="http://www.redlioninn.com/team/chef-brian-alberg/">Chef Brian J. Alberg</a>
	<div class="jobTitle balance-text" style="white-space: normal; float: none; display: block; position: static;">Vice President of Food  <br data-owner="balance-text">and Beverage Operations</div>
</div>
<div class="teamMember">
	<a href="http://www.redlioninn.com/team/brian-butterworth/">Brian Butterworth</a>
	<div class="jobTitle balance-text" style="white-space: normal; float: none; display: block; position: static;">Vice President of Sales</div>
</div>
<div class="teamMember">
	<a href="http://www.redlioninn.com/team/michelle-kotek/">Michele Kotek</a>
	<div class="jobTitle balance-text" style="white-space: normal; float: none; display: block; position: static;">Vice President of Lodging Operations</div>
</div>
<div class="teamMember">
	<a href="http://www.redlioninn.com/team/anne-curtin-nardi/">Anne Curtin-Nardi</a>
	<div class="jobTitle balance-text" style="white-space: normal; float: none; display: block; position: static;">Vice President of Human Resources</div>
</div>
<div class="teamMember">
	<a href="http://www.redlioninn.com/team/andy-huppe/">Andy Huppe</a>
	<div class="jobTitle balance-text" style="white-space: normal; float: none; display: block; position: static;">Chief Financial Officer</div>
</div>
<div class="teamMember">
	<a href="http://www.redlioninn.com/team/stephanie-gravalese/">Stephanie Gravalese</a>
	<div class="jobTitle balance-text" style="white-space: normal; float: none; display: block; position: static;">Director of Marketing and Communications</div>
</div> -->

<?php if(is_page(5325)) { ?> <!-- IS MSHG PAGE -->

<h2>MSHG Management Team</h2>

<?php
$args = array(
	'posts_per_page' => 10,
	'post_type' => 'team',
	'meta_query' => array(
		array(
			'key' => 'team_company',
			'value' => 'mshg',
			'compare' => 'LIKE'
			)
		)
);
query_posts($args);
if ( have_posts() ) : while ( have_posts() ) : the_post();
?>

	 <?php if( ('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']) == get_permalink()) { ?>
                                            <div class="teamMember current-item">
                            <?php } else { ?>
                                            <div class="teamMember">
                            <?php } ?>
		<a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>
		<div class="jobTitle balance-text"><?php the_field('job_title_mshg'); ?></div>
	</div>
<?php endwhile; endif; ?>

<?php } ?>

<?php wp_reset_query(); ?>

<?php if(!is_page(5325)) { ?><!-- IS NOT MSHG PAGE -->

<h2>Our Management Team</h2>

<?php
$args = array(
	'posts_per_page' => 10,
	'post_type' => 'team',
	'meta_query' => array(
		array(
			'key' => 'team_company',
			'value' => 'rli',
			'compare' => 'LIKE'
			)
		)
);
query_posts($args);
if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
	 <?php if( ('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']) == get_permalink()) { ?>
                                            <div class="teamMember current-item">
                            <?php } else { ?>
                                            <div class="teamMember">
                            <?php } ?>
		<a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>
		<div class="jobTitle balance-text"><?php the_field('job_title'); ?></div>
	</div>
<?php endwhile; endif; ?>

<?php } ?>

<?php wp_reset_query(); ?>

</div>

<a class="backtotop" href="#top">Back to Top</a>
</div>

</div><!-- /interiorWrap-->

<?php get_footer(); ?>
