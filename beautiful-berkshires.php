<?php
/*
Template Name: Beautiful Berkshires
*/
?>
        	
<?php get_header(); ?>

    <body id="default" <?php body_class('berkshires'); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        
        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<div id="interiorLeft">
<h1>Exploring <br/>the Beautiful Berkshires</h1>
<?php if (is_archive()): ?>
	<div class="berkshireCatTitle balance-text"><?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); echo $term->name; ?></div>
	<div class="berkshireCatDesc balance-text"><?php echo $term->description; ?></div>
<?php endif; ?>

</div>

<div id="interiorRight">

<?php 
//list terms in a given taxonomy using wp_list_categories (also useful as a widget if using a PHP Code plugin)

$taxonomy     = 'berkshires';
$orderby      = 'name'; 
$show_count   = 0;      // 1 for yes, 0 for no
$pad_counts   = 0;      // 1 for yes, 0 for no
$hierarchical = 1;      // 1 for yes, 0 for no
$title        = '';

$args = array(
  'taxonomy'     => $taxonomy,
  'orderby'      => $orderby,
  'show_count'   => $show_count,
  'pad_counts'   => $pad_counts,
  'hierarchical' => $hierarchical,
  'title_li'     => '',
  'hide_empty'	 => 0,
);
?>

<ul class="berkshires">
<?php wp_list_categories( $args ); ?>
</ul>
</div>

<?php include('includes/menuBoards.php'); ?>

<div id="interiorMiddleWrap">
<div id="interiorMiddle">

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php if (is_archive()) { ?>
		<div class="berkshiresItem">
			<h1 class="balance-text"><?php the_title(); ?></h1>
			<div class="berkshiresInfo">
				<div class="berkshiresAddress1"><?php the_field('address1'); ?></div>
				<div class="berkshiresAddress2"><?php the_field('address2'); ?></div>
				<?php if(get_field('hours')):?>
					<div class="berkshiresHours">
						<?php the_field('hours'); ?>
					</div>
				<?php endif;?>
				<div class="berkshiresURL"><a target="_blank" href="<?php the_field('url');?>"><?php the_field('url');?></a></div>
			<?php the_content(); ?>
			</div>
		</div>
<?php } else { ?>
			<h1 class="balance-text"><?php the_title(); ?></h1>
				<?php the_content(); ?>
<?php } endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
<?php endif; ?>

</div><!-- /interiorMiddle -->
</div>

</div><!-- /interiorWrap-->

<?php get_footer(); ?>