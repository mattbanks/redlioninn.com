<div id="denBottom">

	<div id="social">
		<ul>
        			<li><a class="fb" href="http://www.facebook.com/TheRedLionInnBerkshiresMA">Facebook</a></li>
        			<li><a class="twitter" href="http://twitter.com/TheRedLionInn">Twitter</a></li>
        			<li><a class="tripadvisor" href="http://www.tripadvisor.com/Hotel_Review-g41850-d89873-Reviews-The_Red_Lion_Inn-Stockbridge_Massachusetts.html">TripAdvisor</a></li>
        			<li><a class="blog" href="<?php bloginfo('url'); ?>/blog">Blog</a></li>
        			<li><a class="instagram" href="http://www.instagram.com/redlioninn">Instagram</a></li>
		</ul>
	</div><!-- /social -->


<!--	<img class="denHours" src="<?php bloginfo('stylesheet_directory');?>/images/den/entertainmentHours.png" /> -->

<div class="denHours">
	<?php $recent = new WP_Query("page_id=1884"); while($recent->have_posts()) : $recent->the_post();?>
	<div class="hoursTitle"><?php the_title(); ?></div>
	<?php the_content(); ?>
	<?php endwhile; ?>
</div>


<!--	<a class="learnHistory" href="">Learn the History of the Lion's Den</a> -->


	<img class="denFooter" src="<?php bloginfo('stylesheet_directory');?>/images/den/denFooter.png" />

</div>



        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq=[['_setAccount','UA-2192313-2'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
        <?php wp_footer() ?>
    </body>
</html>
