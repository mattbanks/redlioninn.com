<?php
		$slug = the_slug();
		$args = array(
			'posts_per_page' => 10,
			'post_type' => 'slide',
			'slide-page' => $slug
		);
		query_posts($args);
		if ( have_posts() ) : ?>
		
		<div class="flexslider">
  			<ul class="slides">
  
		<?php while ( have_posts() ) : the_post(); ?>

    <li>
    	<img src="<?php the_field('image'); ?>" alt="<?php the_title(); ?>">
    <?php if(get_field('slideDescription')) { ?>
		<div class="slideDescription"><?php the_field('description'); ?></div>
	<?php } ?>
    </li>

<?php endwhile; ?>

  </ul></div>

<?php endif; ?>
<?php wp_reset_query(); ?>