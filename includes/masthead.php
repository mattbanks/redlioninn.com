<?php if (is_tree(24)) { ?>
				<div id="masthead">
					<a class="homeBanner" href="<?php bloginfo('siteurl'); ?>">The Red Lion Inn - Home</a>
				</div>
<?php } else { ?>
       	<div id="masthead">
                <a class="homeBanner" href="<?php bloginfo('siteurl'); ?>">The Red Lion Inn - Home</a>
        		<ul>
        			<li class="nav1"><a href="<?php bloginfo('siteurl'); ?>/berkshire-accommodations/">Come & Stay</a></li>
        			<li class="nav2"><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/">Wine & Dine</a></li>
        			<li class="nav3"><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/">Meet & Celebrate</a></li>
        			<li class="nav4"><a href="<?php bloginfo('siteurl'); ?>/travel-accommodations-offers/">Special Offers</a></li>
        			<li class="nav5"><a href="<?php bloginfo('siteurl'); ?>/lions-den/">Lion's Den</a></li>
        			<li class="nav6"><a href="<?php bloginfo('siteurl'); ?>/gift-shop">The Gift Shop</a></li>
        		</ul>

                <a class="phoneNumber" href="tel:4132985545">413-298-5545</a>
        		<a class="bookRoom" href="https://booking.ihotelier.com/istay/istay.jsp?HotelID=17402" target="_blank" onClick="_gaq.push(['_trackEvent', 'Book a Room', 'Click']);">BOOK A ROOM</a>
        	</div>
<?php } ?>
