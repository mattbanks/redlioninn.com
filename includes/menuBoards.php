<?php if (is_tree(4) || is_tree(71) || is_tree(182) || is_tree(444)) { ?> <!-- STAY & OFFERS-->
<div id="sideNav">
		<div id="sideNavInner">
			<ul>
			<li><a href="<?php bloginfo('siteurl'); ?>/travel-accommodations-offers/"><span>Specials &</span> Packages</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/meeting-spaces/corporate-negotiated-rates/"><span>Here On</span> Business?</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-accommodations/enhance-your-stay/"><span>Enhance</span> Your Stay</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-accommodations/pets-policy/"><span>Pets Are</span> Welcome</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-accommodations/practical-guest-information/"><span>Practical</span> Info</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/amenities-activities/"><span>Amenities &</span> Activities</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/our-story/simon/#content"><span>our</span> Lobby Cat</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/holiday-celebrations/"><span>holiday</span> <div style="font-size: 80%;">Celebrations</div></a></li>
			</ul>
		</div>
		<a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/" class="bookGroupEvent sideboard">Book a Group Event</a>
</div>
<?php } else if (is_tree(12) && !is_page(89)) { ?> <!-- DINE -->

<div id="sideNavWide">
	<div id="sideNavWideInner">
		<ul>
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/chef-brian-alberg"><span>Chef</span> Brian Alberg</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/our-partners"><span>Our</span> Partners</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/events-calendar/category/wine-dine-events/">Events</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/holiday-celebrations"><span>Holiday</span> Celebrations</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/event-catering-services"><span>RLI</span> Catering</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/private-dining/"><span>Private</span> Dining</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/room-service-menu"><span>Room</span> Service</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/elm-street-market"><span>Elm Street</span> Market</a></li>
		</ul>
	</div>
</div>

<?php } else if (is_home() || is_page_template('index.php')) { ?> <!-- BLOG -->

<div id="sideNav">
	<div id="sideNavInner">
		<ul>
			<li><a class="blogSideNav" href="<?php bloginfo('siteurl'); ?>/blog/chef"><span>Alberg</span> Rants</a></li>
			<li><a class="blogSideNav" href="<?php bloginfo('siteurl'); ?>/blog/wine"><span>Wine</span> Blog</a></li>
			<li><a class="blogSideNav" href="<?php bloginfo('siteurl'); ?>/blog/news"><span>Red Lion Inn</span> News</a></li>
			<li><a class="blogSideNav" href="<?php bloginfo('siteurl'); ?>/blog/simon"><span>Meet</span> Simon</a></li>
		</ul>
	</div>
</div>


<?php } else if (is_tree(16) || is_tree(118) || is_tree(114) || is_tree(116) || is_tree(112)) { ?> <!-- MEET SECTION — MEETING SPACES -->

<div id="sideNavWide" class="sideNavMeetingSpaces">
	<div id="sideNavWideInner" class="sideNavText">
		<ul>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/meeting-spaces/hitchcock-room/">Hitchcock</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/meeting-spaces/townsend-room/">Townsend</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/meeting-spaces/plumb-room/">Plumb</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/meeting-spaces/treadway-room/">Treadway</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/meeting-spaces/meet-stafford-house/">Stafford House</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/meeting-spaces/meet-the-firehouse/">The Firehouse</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/meeting-spaces/meet-maple-glen/">Maple Glen</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/meeting-spaces/meet-stevens-house/">Stevens House</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/meeting-spaces/side-parlor/">Side Parlor</a></li>
		</ul>
	</div>
</div>

<?php } else if (is_tree(89)) { ?> <!-- WINE EXPERIENCE -->

<div id="sideNav">
	<div id="sideNavInner">
		<ul>
			<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/wine-list/"><span>Wine</span> List</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/blog/wine/"><span>Wine</span> Blog</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/events/"><span>Wine</span> Events</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-wine-experience/wine-dinners/"><span>wine</span> Dinners</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-wine-experience/dan-thomas/"><span>meet our</span> Sommelier</a></li>
		</ul>
	</div>
</div>

<?php } else if (is_tree(393) || is_tree(400)) { ?> <!-- COMPANY CULTURE - CORE VALUES -->

<div id="sideNavWide" class="sideNavCoreValues">
	<div id="sideNavWideInner" class="sideNavText">
		<ul>
		<li><a href="<?php bloginfo('siteurl'); ?>/employment-opportunities/company-culture/courtesy">Courtesy</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/employment-opportunities/company-culture/commitment">Commitment</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/employment-opportunities/company-culture/integrity">Integrity</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/employment-opportunities/company-culture/preservation">Preservation</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/employment-opportunities/company-culture/participation">Participation</a></li>
		</ul>
	</div>
</div>

<?php } ?> 