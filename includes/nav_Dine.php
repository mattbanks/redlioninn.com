<div id="navSecond" class="navDine">

		<a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/menu/?locationID=52&mealID=100" class="dine_Browse"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/dine/browseMenu.png" alt="Browse our Menus" /></a>


	<ul>

<?php if (is_page('main-dining-room')) { ?>
		<li><a class="dineNav1" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/main-dining-room/">Main Dining Room</a></li>
		<li><a class="dineNav2" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/widow-binghams-tavern/">Widow Bingham's Tavern</a></li>
		<li class="hdr"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/dine/hdr_MainDining.png"></li>
		<li><a class="dineNav3" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-courtyard/">The Courtyard</a></li>
		<li><a class="dineNav4" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-lions-den/">The Lion's Den</a></li>

<?php } else if (is_page('widow-binghams-tavern')) { ?>
		<li><a class="dineNav1" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/main-dining-room/">Main Dining Room</a></li>
		<li><a class="dineNav2" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/widow-binghams-tavern/">Widow Bingham's Tavern</a></li>
		<li class="hdr"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/dine/hdr_WidowBingham.png"></li>
		<li><a class="dineNav3" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-courtyard/">The Courtyard</a></li>
		<li><a class="dineNav4" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-lions-den/">The Lion's Den</a></li>

<?php } else if (is_page('private-dining-rooms')) { ?>
		<li><a class="dineNav1" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/main-dining-room/">Main Dining Room</a></li>
		<li><a class="dineNav2" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/widow-binghams-tavern/">Widow Bingham's Tavern</a></li>
		<li class="hdr"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/dine/hdr_PrivateDining.png"></li>
		<li><a class="dineNav3" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-courtyard/">The Courtyard</a></li>
		<li><a class="dineNav4" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-lions-den/">The Lion's Den</a></li>

<?php } else if (is_page('the-courtyard')) { ?>
		<li><a class="dineNav1" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/main-dining-room/">Main Dining Room</a></li>
		<li><a class="dineNav2" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/widow-binghams-tavern/">Widow Bingham's Tavern</a></li>
		<li class="hdr"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/dine/hdr_CourtYard.png"></li>
		<li><a class="dineNav3" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-courtyard/">The Courtyard</a></li>
		<li><a class="dineNav4" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-lions-den/">The Lion's Den</a></li>

<?php } else if (is_page('the-lions-den')) { ?>
		<li><a class="dineNav1" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/main-dining-room/">Main Dining Room</a></li>
		<li><a class="dineNav2" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/widow-binghams-tavern/">Widow Bingham's Tavern</a></li>
		<li class="hdr"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/dine/hdr_LionsDen.png"></li>
		<li><a class="dineNav3" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-courtyard/">The Courtyard</a></li>
		<li><a class="dineNav4" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-lions-den/">The Lion's Den</a></li>
<?php } else if (is_page('menu')) { ?>
		<li class="navDineNorm"><a class="dineNav1" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/menu/?locationID=52&mealID=100">Main Dining Room</a></li>
		<li class="navDineNorm"><a class="dineNav2" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/menu/?locationID=129&mealID=137">Widow Bingham's Tavern</a></li>
		<li class="navDineNorm"><a class="dineNav3" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/menu/?locationID=56&mealID=147">The Courtyard</a></li>
		<li class="navDineNorm"><a class="dineNav4" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/menu/?locationID=73&mealID=126">The Lion's Den</a></li>
<?php } else { ?>
		<li class="navDineNorm"><a class="dineNav1" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/main-dining-room/">Main Dining Room</a></li>
		<li class="navDineNorm"><a class="dineNav2" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/widow-binghams-tavern/">Widow Bingham's Tavern</a></li>
		<li class="navDineNorm"><a class="dineNav3" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-courtyard/">The Courtyard</a></li>
		<li class="navDineNorm"><a class="dineNav4" href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-lions-den/">The Lion's Den</a></li>
<?php } ?>

	</ul>

		<a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/reserve-a-table/" class="dine_Reserve"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/dine/reserveTable.png" alt="Reserve a Table" /></a>
</div><!-- /navSecond -->

