<?php
/*
Template Name: STAY (Room Template)
*/
?>

<?php get_header(); ?>

    <body id="stayRoom" <?php body_class($page_slug); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<div id="roomPlate">
<img class="roomTitle" src="<?php bloginfo('stylesheet_directory'); ?>/images/stay/roomPlate-<?php the_ID(); ?>.png">
</div>


<?php include ('includes/sliders-headers.php'); ?>

<div id="interiorMiddleWrap">

<div id="interiorMiddle">

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<!--<img class="roomTitle" src="<?php bloginfo('stylesheet_directory'); ?>/images/stay/roomTitle-<?php the_ID(); ?>.png">-->

<div class="roomTagline"><?php the_field('tagline'); ?></div>

<h1><?php the_title(); ?></h1>

<!--<a class="comparisonChart" href="#">Comparison Chart</a>-->

<a class="checkRates" href="https://booking.ihotelier.com/istay/istay.jsp?HotelID=17402">
<img alt="Reserve a Private Dining Room" src="<?php bloginfo('stylesheet_directory'); ?>/images/stay/checkRates.png">
</a>

<?php the_content(); ?>

<?php if(get_field('minutes_from_lobby')) { ?><div class="roomDistance"><span><span class="num"><?php the_field('minutes_from_lobby'); ?></span>-minute</span> walk from lobby</div><?php } ?>

<?php endwhile; endif; ?>

</div><!-- /interiorMiddle -->
</div>

<div id="interiorLeft">

<?php if(is_tree(46)) { ?>
	<ul class="tertiaryNav roomTertiary">
		<li><a class="id48" href="<?php bloginfo('siteurl');?>/berkshire-accommodations/main-inn/suites/">Suites</a></li>
		<li><a class="id50" href="<?php bloginfo('siteurl');?>/berkshire-accommodations/main-inn/superior-rooms/">Superior Rooms</a></li>
		<li><a class="id52" href="<?php bloginfo('siteurl');?>/berkshire-accommodations/main-inn/deluxe-rooms/">Deluxe Rooms</a></li>
		<li><a class="id54" href="<?php bloginfo('siteurl');?>/berkshire-accommodations/main-inn/bb-rooms/">B<span class="amp">&</span>B Rooms</a></li>
	</ul>
<?php } else if(is_tree(366) || is_tree(30) || is_tree(32) || is_tree(36) || is_tree(20) || is_tree(42)) { ?>
		<ul class="tertiaryNav roomTertiary">
		<li><a class="id30" href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/mcgregor-house/">M<span>c</span>Gregor House</a></li>
		<li><a class="id32" href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/2-maple/">2 Maple</a></li>
		<li><a class="id56" href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/the-firehouse/">The Firehouse</a></li>
		<li><a class="id34" href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/meadowlark/">Meadowlark</a></li>
		<li><a class="id36" href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/obrien-house/">O'Brien House</a></li>
		<li><a class="id38" href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/stevens-house/">Stevens House</a></li>
		<li><a class="id20" href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/stafford-house/">Stafford House</a></li>
		<li><a class="id40" href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/yellow-cottage/">Yellow Cottage</a></li>
		<li><a class="id42" href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/maple-glen/">Maple Glen</a></li>
	</ul>
<?php } ?>


</div>

<div id="interiorRight">

	<div id="sideNav" class="roomSide">
		<div id="sideNavInner">
			<ul>
			<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-accommodations/enhance-your-stay/"><span>Enhance</span> Your Stay</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-accommodations/practical-guest-information/"><span>Practical</span> Info</a></li>
			<li><a href="<?php bloginfo('siteurl')?>/berkshire-dining/room-service-menu/"><span>Room Service</span> Menu</a></li>
			</ul>
		</div>
		<a class="bookGroupEvent sideboard">Book a Group Event</a>
	</div>

</div>

</div><!-- /interiorWrap-->

<?php get_footer(); ?>
