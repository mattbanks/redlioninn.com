<?php
/*
Template Name: STAY
*/
?>

<?php get_header(); ?>

    <body id="stay" <?php body_class('stay-map'); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<h1 class="pageHeader">Come & Stay</h1>

<?php include('includes/menuBoards.php'); ?>

<div id="aboutYourStay"><a href="#content">About Your Stay</a></div>

<div id="stayMap">
	<ul id="stayMap_Main">
		<li class="deluxerooms"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/main-inn/deluxe-rooms/">Deluxe Rooms</a></li>
		<li class="superiorrooms"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/main-inn/superior-rooms/">Superior Rooms</a></li>
		<li class="suites"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/main-inn/suites/">Suites</a></li>
		<li class="bbrooms"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/main-inn/bb-rooms/">B&B Rooms</a></li>
		<li class="maininn"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/main-inn/">Main Inn</a></li>
	</ul>
	<ul id="stayMap_Guest">
		<li class="firehouse"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/the-firehouse/">The Firehouse</a></li>
		<li class="mapleglen"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/maple-glen/">Maple Glen</a></li>
		<li class="maple2"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/2-maple/">2 Maple</a></li>
		<li class="mcgregor"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/mcgregor-house/">McGregor House</a></li>
		<li class="obrien"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/obrien-house/">O'Brien House</a></li>
		<li class="cottage"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/yellow-cottage/">Yellow Cottage</a></li>
		<li class="stevens"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/stevens-house/">Stevens House</a></li>
		<li class="stafford"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/stafford-house/">Stafford House</a></li>
		<li class="meadowlark"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/meadowlark/">Meadowlark</a></li>
		<li class="guesthouses"><a href="<?php bloginfo('siteurl');?>/berkshire-accommodations/guest-houses/">Guest Houses</a></li>
	</ul>

</div><!-- /stayMap -->




<a name="content"></a>

<div id="interiorMiddleWrap">

<div id="interiorMiddle">

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php the_content(); ?>
<?php endwhile; endif; ?>

</div><!-- /interiorMiddle -->


</div><!-- /interiorWrap-->

<?php get_footer(); ?>
