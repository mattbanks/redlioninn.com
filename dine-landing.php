<?php
/*
Template Name: Dine (Landing)
*/
?>

<?php get_header(); ?>

    <body id="dine">
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<?php include('includes/nav_Dine.php'); ?>

<div id="interiorWrap">

<?php include('includes/sliders-headers.php'); ?>

<?php include('includes/menuBoards.php'); ?>

<div id="interiorMiddle">

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php the_content(); ?>
<?php endwhile; endif; ?>

</div><!-- /interiorMiddle -->

<div id="interiorLeft">

	<a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-wine-experience" class="wineEx">The Wine Experience</a>

	<a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/" class="dine_Private"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/dine/planPrivate.png" alt="Plan a Private Event" /></a>

</div>

</div><!-- /interiorWrap-->

<?php get_footer(); ?>
