<?php
    if($_REQUEST['locationID']) {
        $_SESSION['locationID'] = $_REQUEST['locationID'];
        $LocationName = $wpdb->get_results("SELECT term_id, name FROM $wpdb->terms WHERE term_id = '".$_SESSION['locationID']."'");
        $_SESSION['mealName'] = $mealNameFetch[0]->name;
        $_SESSION['mealID'] = "";
    }
    if(stristr($_SERVER['REQUEST_URI'], "/lions-den-menu")) {
        $_SESSION['locationID'] = 73;
    }
    if($_REQUEST['mealID']) {
        $_SESSION['mealID'] = $_REQUEST['mealID'];
        if($_SESSION['mealID']=="winelist") {
            $_SESSION['mealName'] = "Wine List";
        } else {
            $mealNameFetch = $wpdb->get_results("SELECT name FROM $wpdb->terms WHERE term_id = '".$_SESSION['mealID']."'");
            $_SESSION['mealName'] = $mealNameFetch[0]->name;
        }
    }
        
    $mealIDArray = array();
    $mealNameArray = array();
    if(isset($_SESSION['locationID'])) {
        $mealIDArray = $wpdb->get_results("SELECT term_id FROM $wpdb->term_taxonomy WHERE parent = '".$_SESSION['locationID']."'");
        $mealInStatement = "";
        for($x=0; $x<count($mealIDArray); $x++) {
            if($x!=0) {
                $mealInStatement .= ", ";
            }
            $mealInStatement .= $mealIDArray[$x]->term_id;
        }
        $mealIDArray = $wpdb->get_results("SELECT term_id FROM $wpdb->terms WHERE term_id IN(".$mealInStatement.") order by term_order asc");
        $mealInStatement = "";
        for($x=0; $x<count($mealIDArray); $x++) {
            if($x!=0) {
                $mealInStatement .= ", ";
            }
            $mealInStatement .= $mealIDArray[$x]->term_id;
        }
        $mealNameArray = $wpdb->get_results("SELECT term_id, name FROM $wpdb->terms WHERE term_id IN(".$mealInStatement.") order by term_order asc");
    }
    
    $courseIDArray = array();
    $courseNameArray = array();
    $courseMenuItemArray = array();
    if(isset($_SESSION['mealID'])) {
        if($_SESSION['mealID']!="winelist") {
            $courseIDArray = $wpdb->get_results("SELECT term_id, term_taxonomy_id, description FROM $wpdb->term_taxonomy WHERE parent = '".$_SESSION['mealID']."'");
            $courseInStatement = "";
            for($x=0; $x<count($courseIDArray); $x++) {
                if($x!=0) {
                    $courseInStatement .= ", ";
                }
                $courseInStatement .= $courseIDArray[$x]->term_id;
            }
            $courseIDOrderArray = $wpdb->get_results("SELECT term_id FROM $wpdb->terms WHERE term_id IN(".$courseInStatement.") order by term_order asc");
            $courseIDTmpArr = array();
            for($x=0; $x<count($courseIDOrderArray); $x++) {
                for($q=0; $q<count($courseIDArray); $q++) {
                    if($courseIDOrderArray[$x]->term_id == $courseIDArray[$q]->term_id) {
                        $courseIDTmpArr[] = $courseIDArray[$q];
                    }
                }
            }
            $courseIDArray = $courseIDTmpArr;
            $courseNameArray = $wpdb->get_results("SELECT term_id, name FROM $wpdb->terms WHERE term_id IN(".$courseInStatement.") order by term_order asc");

            for($z=0; $z<count($courseIDArray); $z++) {
                $menuItemInfoArray = array();
                $menuItemIDArray = $wpdb->get_results("SELECT object_id FROM $wpdb->term_relationships WHERE term_taxonomy_id = '".$courseIDArray[$z]->term_taxonomy_id."'");
                $menuItemInStatement = "";
                for($x=0; $x<count($menuItemIDArray); $x++) {
                    if($x!=0) {
                        $menuItemInStatement .= ", ";
                    }
                    $menuItemInStatement .= $menuItemIDArray[$x]->object_id;
                }
                $menuItemArray = $wpdb->get_results("SELECT ID, post_title FROM $wpdb->posts WHERE ID IN(".$menuItemInStatement.") and post_status='publish' ");
                $menuItemIDTmpArray = array();
                for($x=0; $x<count($menuItemArray); $x++) {
                    for($q=0; $q<count($menuItemIDArray); $q++) {
                        if($menuItemArray[$x]->ID==$menuItemIDArray[$q]->object_id) {
                            $menuItemIDTmpArray[] = $menuItemIDArray[$q];
                        }
                    }
                }
                $menuItemIDArray = $menuItemIDTmpArray;
                for($x=0; $x<count($menuItemIDArray); $x++) {
                    $tmpCourseInfoArray = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE post_id = '".$menuItemArray[$x]->ID."'");
                    for($y=0; $y<count($tmpCourseInfoArray); $y++) {
                        switch($tmpCourseInfoArray[$y]->meta_key) {
                            case "price1";
                                $menuItemInfoArray[$x]['price1'] = $tmpCourseInfoArray[$y]->meta_value;
                                break;
                            case "price1_label":
                                $menuItemInfoArray[$x]['price1_label'] = $tmpCourseInfoArray[$y]->meta_value;
                                break;
                            case "price2":
                                $menuItemInfoArray[$x]['price2'] = $tmpCourseInfoArray[$y]->meta_value;
                                break;
                            case "price2_label":
                                $menuItemInfoArray[$x]['price2_label'] = $tmpCourseInfoArray[$y]->meta_value;
                                break;
                            case "description":
                                $menuItemInfoArray[$x]['description'] = $tmpCourseInfoArray[$y]->meta_value;
                                break;
                        }
                    }
                }
                $courseMenuItemArray[$z]['menuItemIDArray'] = $menuItemIDArray;
                $courseMenuItemArray[$z]['menuItemArray'] = $menuItemArray;
                $courseMenuItemArray[$z]['menuItemInfoArray'] = $menuItemInfoArray;
            }
        }
    }
    
?>
    
