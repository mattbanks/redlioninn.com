<?php
/*
Template Name: Packages (Landing)
*/
?>
        	
<?php get_header(); ?>

    <body id="offers" <?php body_class($page_slug); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        
        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<div class="offersHeader">
<div class="lodgingTitle"><a href="<?php bloginfo('siteurl'); ?>/travel-accommodations-offers/">Lodging Specials</a></div>
<div class="packagesTitle active">Packages</div>
</div>

	<?php include('includes/sliders-headers.php'); ?>


<?php include('includes/menuBoards.php'); ?>

<div id="packagesMiddle">


<?php if (have_posts()) : while (have_posts()) : the_post();?>
<h1><?php the_title(); ?></h1>
<div class="calloutTitle">Convenient packages at a great value!</div>
<?php the_content(); ?>
<?php endwhile; endif; ?>

<?php
$today = date('Ymd', strtotime('-1 day'));
$args = array(
	'meta_key' => 'expiration',
	'meta_value' => $today,
	'meta_compare' => '>',
	'posts_per_page' => 20,
	'post_type' => 'packages'
);
query_posts($args);
if ( have_posts() ) : while ( have_posts() ) : the_post();
?>

<div class="package">
	<div class="packageTitle"><?php the_title(); ?></div>

<?php if(get_field('image')) { ?>
	<div class="packageDetailsBlock">
		<div class="packageSubtitle"><?php the_field('subtitle'); ?></div>
		<img src="<?php the_field('image'); ?>">
	</div>
	<div class="packageDesc"><?php the_field('description'); ?></div>
<?php } else { ?>
	<div class="packageDetailsBlock">
		<div class="packageSubtitle"><?php the_field('subtitle'); ?></div>
		<div class="packageDesc"><?php the_field('description'); ?></div>
	</div>
<?php } ?>

	<div class="packagePriceBlock">
		<div class="packagePrice">
			<?php if(get_field('price_1_original')) { ?>
				<div class="originalPrice"><sup>$</sup><?php the_field('price_1_original'); ?></div>
			<?php } ?>
			<span>$<?php the_field('price_1'); ?></span>
			<?php the_field('price_1_days'); ?>
		</div>

	<?php if(get_field('price_2')) { echo '<div class="packagePrice">' ; } ?>
	<?php if(get_field('price_2_original')) { ?>
		<div class="originalPrice"><sup>$</sup><?php the_field('price_2_original'); ?></div>
	<?php } ?>
	<?php if(get_field('price_2'))
					{
							echo '<span>$' . get_field('price_2') . '</span>' . get_field('price_2_days') . '</div>';
						}
				?>
	<?php if(get_field('price_3_original')) { ?>
		<div class="originalPrice"><sup>$</sup><?php the_field('price_3_original'); ?></div>
	<?php } ?>				
		<?php if(get_field('price_3'))
					{
							echo '<div class="packagePrice"><span>$' . get_field('price_3') . '</span>' . get_field('price_3_days') . '</div>';
						}
				?>
			
	</div>

	<?php if(get_field('ihotelier_link')) { ?><a class="checkOffer" target="_blank" href="<?php the_field('ihotelier_link'); ?>">Check Rates and Availability</a><?php } else { ?> <div class="reserveText">To reserve, please call 413.298.1690 or email <a href="mailto:reservations@redlioninn.com">reservations@redlioninn.com</a></div> <?php } ?>
	<div class="packageSmall"><?php the_field('small'); ?></div>
</div>

<?php endwhile; endif; ?>


	
</div><!-- /offersMiddle -->

</div><!-- /interiorWrap-->

<?php get_footer(); ?>