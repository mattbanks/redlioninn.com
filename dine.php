<?php
/*
Template Name: Dine Section
*/
?>
        	
<?php get_header(); ?>

    <body id="dine" <?php body_class('page-dine'); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        
        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<?php include('includes/nav_Dine.php'); ?>

<div id="interiorWrap">

<?php include('includes/sliders-headers.php'); ?>

<?php include('includes/menuBoards.php'); ?>

<div id="interiorLeft">
<?php if (is_page(1011)) { ?>
<div id=OT_searchWrapperAll><link href="http://www.opentable.com/ism/feed_transparent_vertical_alt.css" rel="stylesheet" type="text/css" /><script type="text/javascript" src="http://www.opentable.com/ism/default.aspx?rid=23497&restref=23497&mode=vert-transparent-black"></script><noscript id="OT_noscript"><a href="http://www.opentable.com/the-red-lion-inn-reservations-stockbridge?rtype=ism&restref=23497" >Reserve Now On OpenTable.com</a></noscript><div id="OT_logoLink"><a href="http://www.opentable.com/the-red-lion-inn-reservations-stockbridge?rtype=ism&restref=23497" >The Red Lion Inn Reservations</a></div><div id="OT_logo"><a href="http://www.opentable.com/home.aspx?restref=23497&rtype=ism" title="Powered By OpenTable"><img src="http://www.opentable.com/img/buttons/Otlogo.gif" id="OT_imglogo" alt="Restaurant Management Software" /></a></div></div>
<?php } else { ?>

	<a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/the-wine-experience" class="wineEx">The Wine Experience</a>

	<a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/" class="dine_Private"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/dine/planPrivate.png" alt="Plan a Private Event" /></a>

<?php } ?>
</div>


<div id="interiorMiddle">

<?php if (is_page('main-dining-room')) { ?>
		<div class="viewMenu"><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/menu/?locationID=52&mealID=100"><span>view</span>Main Dining Room Menus</a></div>
<?php } else if (is_page('widow-binghams-tavern')) { ?>
		<div class="viewMenu"><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/menu/?locationID=129&mealID=137"><span>view</span>Widow Bingham's Tavern Menus</a></div>
<?php } else if (is_page('the-courtyard')) { ?>
		<div class="viewMenu"><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/menu/?locationID=56&mealID=147"><span>view</span>The Courtyard Menus</a></div>
<?php } else if (is_page('the-lions-den')) { ?>
		<div class="viewMenu"><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/menu/?locationID=73&mealID=126"><span>view</span>The Lion's Den Menus</a></div>
<?php } ?>


<?php if (have_posts()) : while (have_posts()) : the_post();?>
<h1><?php the_title(); ?></h1>
<?php the_content(); ?>
<?php endwhile; endif; ?>

</div><!-- /interiorMiddle -->


</div><!-- /interiorWrap-->

<?php get_footer(); ?>