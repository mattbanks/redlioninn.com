<?php
/*
Template Name: No Slider
*/
?>

<?php get_header(); ?>

    <body id="blog" <?php body_class($page_slug); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">


<div id="interiorMiddleWrap">
<div id="interiorMiddle">

<div class="blogHeader">
	<?php if (is_category('chef')) { ?>
		<h1>alberg <span>rants</span></h1>
	<?php } else if (is_category('simon')) { ?>
		<h1>meet <span>simon</span></h1>
	<?php } else if (is_category('wine')) { ?>
		<h1>wine <span>blog</span></h1>
	<?php } else if (is_category('news')) { ?>
		<h1>red lion inn <span>news</span></h1>
	<?php } else if (is_category('whats-happening')) { ?>
		<h1>what's <span>happening?</span></h1>
	<?php } else { ?>
		<h1>red lion inn <span>blog</span></h1>
		<p><?php bloginfo('description'); ?></p>
	<?php } ?>

	<?php if ( is_category() ) { ?>
		<?php echo category_description(); ?>
		<p><a href="<?php bloginfo('siteurl'); ?>/blog">Read all the blogs &raquo;</a></p>
	<?php } ?>
</div>


<?php global $wp_query;
                    query_posts(
                       array_merge(
                           $wp_query->query,
                           array(
                               'orderby' => 'post_date',
                               'order' => 'desc'
                           )
                       )
                    );
               ?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>



<div class="blogEntry">
	<div class="blogDate">
		<span class="blogMonth"><?php the_time('F'); ?></span>
		<span class="blogDay"><?php the_time('j'); ?></span>
	</div>
	<a href="<?php the_permalink(); ?>"><h1 class="balance-text"><?php the_title(); ?></h1></a>
	<?php the_content(); ?>
</div>

<?php endwhile; endif; ?>

</div><!-- /interiorMiddle -->
</div>

<div id="sideNav">
	<div id="sideNavInner">
		<ul>
			<li><a href="<?php bloginfo('siteurl'); ?>/blog/chef"><span>Alberg</span> Rants</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/blog/wine"><span>Wine</span> Blog</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/blog/news"><span>Red Lion Inn</span> News</a></li>
			<li><a href="<?php bloginfo('siteurl'); ?>/blog/whats-happening"><span>What's</span> Happening?</a></li>
			<!--<li><a href="<?php bloginfo('siteurl'); ?>/blog/simon"><span>Meet</span> Simon</a></li>-->
		</ul>
	</div>
</div>

</div><!-- /interiorWrap-->

<?php get_footer(); ?>
