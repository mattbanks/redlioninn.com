(function($) {

	// Initiate FlexSlider
	$(window).load(function() {
		$('.flexslider').flexslider();
	});

	// Simple Weather
	$(function() {
		$.simpleWeather({
			zipcode: '01262',
			woeid: '',
			location: '',
			unit: 'f',
			success: function(weather) {
				var html = '<img class="weatherImg" src="'+weather.image+'">';
				html += '<div class="weatherTemp">'+weather.temp+'&deg; '+weather.units.temp+'</div>';
				html += '<div class="weatherCity">in '+weather.city+', '+weather.region+'</div>';

				$("#weather").html(html);
			},
			error: function(error) {
				$("#weather").html('<p>'+error+'</p>');
			}
		});
	});


	// for Interior Wrap, setup things to work properly
	var $interiorWrap = $('#interiorWrap'),
		$rightSide = $('#interiorRight'),
		$leftSide = $('#interiorLeft');

	if ( $interiorWrap.length && $rightSide.length && $leftSide.length ) {
		var	rightTopMarginStart = Number( $rightSide.css('margin-top').replace('px','') ),
			leftTopMarginStart = Number( $leftSide.css('margin-top').replace('px','') );
	}

	var fixInteriorWrappedMargins = function() {
		if ( $interiorWrap.length && $rightSide.length && $leftSide.length && !$('body').hasClass('berkshires') ) {
			if ( $(window).width() > 1023 ) {
				var midHeight = $('#interiorMiddle').outerHeight(),
					rightTopMargin = midHeight - rightTopMarginStart,
					leftTopMargin = midHeight - leftTopMarginStart;

				$rightSide.css('margin-top', - rightTopMargin);
				$leftSide.css('margin-top', - leftTopMargin);
			}
			else {
				$rightSide.css('margin-top', rightTopMarginStart);
				$leftSide.css('margin-top', leftTopMarginStart);
			}
		}
	};

	$(document).ready(function() {
		fixInteriorWrappedMargins();
	});

	$(window).resize(function() {
		fixInteriorWrappedMargins();
	});


	// change book a room color on mobile to override multiple css declarations
	var bookRoomLinkColor = function() {
		var $bookRoom = $('.bookRoom');

		if ( $(window).width() < 801 ) {
			$bookRoom.each( function() {
				this.style.setProperty( 'color', 'white', 'important' )
			});
		}
		else {
			$bookRoom.each( function() {
				this.style.setProperty( 'color', 'black', 'important' )
			});
		}
	};

	$(document).ready(function() {
		bookRoomLinkColor();
	});

	$(window).resize(function() {
		bookRoomLinkColor();
	});

})(jQuery);
