<?php
/*
Template Name: No Slider
*/
?>
        	
<?php get_header(); ?>

    <body id="default" <?php body_class($page_slug); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        
        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<div id="interiorMiddleWrap">

<div id="page404">
	<span>Our Apologies.</span>
	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/404.png" />
	<p>The page you are looking for cannot be found.</p>
	<a href="<?php bloginfo('url');?>">return to homepage</a>
</div>

</div><!-- /interiorWrap-->

<?php get_footer(); ?>