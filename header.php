<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php bloginfo('name'); ?> <?php wp_title( '|' ); ?> </title>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

<!--[if lte IE 8]>
    <style type='text/css'>
  #interiorMiddleWrap { background: none !important; }
  body#home { background-color: #fbf5de; }
  #interiorMiddle, #interiorRight, .slideDescription, .offerDays, .packageDesc, .packageSmall, span.back, div#weather, .OT_subtitle, ul.children li.cat-item, .berkshireCatDesc, .artistProfile, .webLink a, #propertyMap .mapPoint:hover, #propertyMap .mapAlt:hover, .woocommerce-tabs, .price  { font-family: 'Arial Narrow', Arial, sans-serif !important; font-size: 85% !important; }
  #bottomWrap { background:transparent;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#DDFBF4DF,endColorstr=#DDFBF4DF); }
  #topWrap { background:transparent;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#DDFBF4DF,endColorstr=#DDFBF4DF); }
    </style>
<![endif]-->

<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->

<style type="text/css">
    /* Apply (proposed) CSS style. Plugin looks for elements with class named "balance-text" */
    .balance-text {
        text-wrap: balanced;
    }
    </style>

<?php if (is_tree(24)) { ?>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style-den.css">
<?php } else if (is_shop() || is_woocommerce() || is_cart() || is_checkout() || is_page('gift-shop-coming-soon') || is_page_template('woocommerce-pages.php')) { ?>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style-shop.css">
<?php } else if (is_page(201)) { ?>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style-map.css">
<?php } else if (is_tree(330) || is_page(354) || is_tree(334) || is_tree(417)|| is_tree(369) || is_taxonomy('berkshires') || is_tree(393)) { ?>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style-footercontent.css">
<?php } ?>

<?php
$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
if (false !== strpos($url,'events-calendar/category/lions-den')) { ?>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style-den.css">
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/tribe-events/events.css">
        <link rel="stylesheet" media="print" href="<?php bloginfo('stylesheet_directory'); ?>/tribe-events/den-events-print.css">
<?php } ?>

<?php wp_head() ?>
    </head>


