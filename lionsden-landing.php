<?php
/*
Template Name: Lion's Den (Landing)
*/
?>
        	
<?php get_header(); ?>

    <body id="lionsden" <?php body_class('denHome'); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

         <?php include('includes/masthead.php'); ?> 
     
        <div id="denWrap">


		<a href="<?php bloginfo('url'); ?>/lions-den/"><img class="denLogo" src="<?php bloginfo('stylesheet_directory');?>/images/den/lionsDenLogo.png"></a>
		
<!-- BODY COPY GOES HERE -->

<ul id="denNav">
<li><a class="calendar" href="<?php bloginfo('url'); ?>/events-calendar/category/lions-den/">Entertainment Calendar</a></li>
<li><a class="menu-bar" href="<?php bloginfo('url'); ?>/den/lions-den-menu/?mealID=126">Lion's Den Menu & Bar</a></li>
<li><a class="profiles" href="<?php bloginfo('url'); ?>/lions-den/artist-profiles/">Artist Profiles</a></li>
</ul>


<?php include('includes/denfooter.php'); ?>

