<?php
/*
Template Name: Shop (Landing)
*/
?>

<?php get_header(); ?>

    <body id="shop" >
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<?php include('includes/sliders-headers.php'); ?>

<img id="shopLandingPlate" src="<?php bloginfo('stylesheet_directory'); ?>/images/shop/shop-landing.png" />

<div id="interiorMiddle">

	<div class="shopHours">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/images/shop/shopHours.png" />
		<span><?php the_field('shop_days_1'); ?></span>
		<span class="shopTimes"><?php the_field('shop_hours_1'); ?></span>
		<span><?php the_field('shop_days_2'); ?></span>
		<span class="shopTimes"><?php the_field('shop_hours_2'); ?></span>
		<span class="shopDivider">&nbsp;</span>
	</div>



</div><!-- /interiorMiddle -->

<div id="interiorRight" class="shop">
<a href="http://www.redlioninn.com/shop"><img src="http://www.redlioninn.com/wp-content/themes/redlioninn/images/shop/shopNow.png"></a>
</div>

<div id="interiorLeft">
	<div class="shopHours">
		<span>For more info call:</span>
		<span class="shopTimes">413-298-1623</span>
	</div>
</div>



</div><!-- /interiorWrap-->

<?php get_footer(); ?>
