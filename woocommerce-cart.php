<?php
/*
Template Name: Woo Commerce Cart
*/
?>

<?php get_header(); ?>

    <body id="shop" <?php body_class($page_slug); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

<div id="shopWrap">

	<div id="shopMeta">
		<div id="shopLogo">
			<a href="<?php bloginfo('siteurl');?>">Shop</a>
		</div>


		<ul class="tertiaryNav"><?php $args = array( 'taxonomy' => 'product_cat' );
$terms = get_terms('product_cat', $args);

    $count = count($terms); $i=0;
    if ($count > 0) {
        foreach ($terms as $term) {
            $i++;
            $term_list .= '<li><a href="/product-category/' . $term->slug . '" title="' . sprintf(__('View all post filed under %s', 'my_localization_domain'), $term->name) . '">' . $term->name . '</a></li>';
        }
        echo $term_list;
    }
?>
</ul>




		<ul id="shopLinks">
			<li><a href="<?php bloginfo('url');?>/gift-shop-mission">Gift Shop Mission</a></li>
			<li><a href="<?php bloginfo('url');?>/customer-service">Customer Service</a></li>
			<li><a href="<?php bloginfo('url');?>/shipping-returns">Shipping & Returns</a></li>
			<li><a href="<?php bloginfo('url');?>/privacy-policy">Privacy Policy</a></li>
			<li><a href="<?php bloginfo('url');?>/contact-the-gift-shop">Contact the Gift Shop</a></li>
			<li style="margin-top: 40px;"><img src="<?php bloginfo('template_directory');?>/images/shop/shopAddress.png"></li>
		</ul>

<div id="SSLseal">
<!-- GeoTrust QuickSSL [tm] Smart  Icon tag. Do not edit. -->
<script language="javascript" type="text/javascript" src="//smarticon.geotrust.com/si.js"></script>
<!-- end  GeoTrust Smart Icon tag -->
</div>

	</div> <!-- /shopMeta -->


<div id="shopContent">

	<div id="shopHdr"><img src="<?php bloginfo('template_directory');?>/images/shop/shopHdr-cart.png"></div>

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<h1 class="balance-text"><?php the_title(); ?></h1>
<?php the_content(); ?>
<?php endwhile; endif; ?>

</div>

	<div id="shopTools">
		<div class="cartLink">
			Your Shopping Cart
			<a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>">(<?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?>)</a>
		</div>

		<div class="custSvc">
		Customer Service<br/>800.445.9000
		</div>

        	<div id="emailSubscribe">

        	<!-- BEGIN: Constant Contact Basic Opt-in Email List Form -->
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:2;">
<input type="hidden" name="llr" value="9htafjcab">
<input type="hidden" name="m" value="1101966363898">
<input type="hidden" name="p" value="oi">
<input type="text" name="ea" size="20" value="" class="emailAddress">
<input type="submit" name="go" value="submit" class="submit">
</form>
<!-- END: Constant Contact Basic Opt-in Email List Form -->
<!-- BEGIN: SafeSubscribe
<div align="center" style="padding-top:5px;">
<img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt=""/>
</div>

<div align="center" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;">
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank" rel="nofollow">Email Marketing</a> you can trust
</div>
END: Email Marketing you can trust -->


        	</div><!-- /emailSubscribe -->

	<div id="social">
	<ul>
		<li><a class="fb" href="http://www.facebook.com/TheRedLionInnBerkshiresMA">Facebook</a></li>
		<li><a class="twitter" href="http://twitter.com/TheRedLionInn">Twitter</a></li>
		<li><a class="tripadvisor" href="http://www.tripadvisor.com/Hotel_Review-g41850-d89873-Reviews-The_Red_Lion_Inn-Stockbridge_Massachusetts.html">TripAdvisor</a></li>
		<li><a class="blog" href="<?php bloginfo('url'); ?>/blog">Blog</a></li>
		<li><a class="instagram" href="http://www.instagram.com/redlioninn">Instagram</a></li>
	</ul>
</div><!-- /social -->


	</div><!-- /shopTools -->



</div><!-- /shopWrap -->


        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq=[['_setAccount','UA-2192313-2'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
        <?php wp_footer() ?>
    </body>
</html>
