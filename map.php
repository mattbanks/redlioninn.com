<?php
/*
Template Name: Footer: Property Map
*/
?>

<?php get_header(); ?>

    <body id="map" <?php body_class($page_slug); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<div id="propertyMap">

		<a class="scrollDown" href="#down">Scroll Down For More</a>

	<div class="mapPoint map_Firehouse">
		<div class="mapInfo">
			<h1>The Firehouse</h1>
			<p>The turn-of-the-century firehouse used by the town of Stockbridge until the 1950s  has been converted into unique overnight accommodations.</p>
			<a class="bookIt" href="https://booking.ihotelier.com/istay/istay.jsp?HotelID=17402">Book It</a>
			<a class="learnMore" href="<?php bloginfo('url'); ?>/berkshire-accommodations/guest-houses/the-firehouse/">Learn More</a>
		</div>
	</div>

	<div class="mapPoint map_Obrien">
		<div class="mapInfo">
			<h1>O'Brien House</h1>
			<p>The former home of Miss Josephine O’Brien, a long-time teacher in the Stockbridge village school.</p>
			<a class="bookIt" href="https://booking.ihotelier.com/istay/istay.jsp?HotelID=17402">Book It</a>
			<a class="learnMore" href="<?php bloginfo('url'); ?>/berkshire-accommodations/guest-houses/obrien-house/">Learn More</a>
		</div>
	</div>

	<div class="mapAlt popB map_2Maple">
		<div class="mapInfo">
			<h1>2 Maple</h1>
			<p>Build around 1830, Two Maple is located on the corner of Maple and Elm Street, and once served as the rectory for St. Joseph's Church.</p>
			<a class="bookIt" href="https://booking.ihotelier.com/istay/istay.jsp?HotelID=17402">Book It</a>
			<a class="learnMore" href="<?php bloginfo('url'); ?>/berkshire-accommodations/guest-houses/2-maple/">Learn More</a>
		</div>
	</div>


	<div class="mapPoint map_Elm">
		<div class="mapInfo">
			<h1>Elm Street Market</h1>
			<p>Our favorite market. A local gathering place with an old-fashioned breakfast & lunch counter, deli and butcher shop. Open seven days a week!</p>
			<a class="learnMore" href="<?php bloginfo('url'); ?>/dine/elm-street-market/">Learn More</a>
		</div>
	</div>

<!--		<div class="mapPoint map_Heather">
		<div class="mapInfo">
			<h1>Heather Cottage</h1>
			<p>Our favorite local market. A local gathering place with an old-fashioned breakfast & lunch counter, deli and butcher shop. Open seven days a week!</p>
			<a class="bookIt" href="https://booking.ihotelier.com/istay/istay.jsp?HotelID=17402">Book It</a>
			<a class="learnMore" href="#">Learn More</a>
		</div>
	</div>
-->

		<div class="mapPoint popE map_Pool">
		<div class="mapInfo">
			<h1>Pool & Pool House</h1>
			<p>Our four-season heated pool and hot tub feature a heated deck so that our guests can swim and relax in whatever heat or cold weather New England sends our way.</p>
			<a class="learnMore" href="http://www.redlioninn.com/amenities-activities/year-round-pool-hot-tub/">Learn More</a>
		</div>
	</div>


		<div class="mapPoint popA map_Yellow">
		<div class="mapInfo">
			<h1>Yellow Cottage</h1>
			<p>For families who want just that extra bit of privacy. This enticing bungalow has it’s own private yard and garden&mdash;the perfect setting for a lazy summer day.</p>
			<a class="learnMore" href="<?php bloginfo('url'); ?>/berkshire-accommodations/guest-houses/yellow-cottage/">Learn More</a>
		</div>
	</div>

	<div class="mapPoint popA map_RLI">
		<div class="mapInfo">
			<h1>Main Inn</h1>
			<p>Established circa 1773, The Inn's historic main building offers 81 individually decorated, antique-filled guest rooms and suites.</p>
			<a class="bookIt" href="https://booking.ihotelier.com/istay/istay.jsp?HotelID=17402">Book It</a>
			<a class="learnMore" href="<?php bloginfo('url'); ?>/berkshire-accommodations/main-inn/">Learn More</a>
		</div>
	</div>

	<div class="mapPoint popA map_Courtyard">
		<div class="mapInfo">
			<h1>The Courtyard</h1>
			<p>Guests and locals alike flock to the Courtyard in the summer to experience our fine dining in this comfortable and casual outdoor setting.</p>
			<a class="learnMore" href="<?php bloginfo('url'); ?>/dine/the-courtyard/">Learn More</a>
		</div>
	</div>

	<div class="mapPoint map_McGregor">
		<div class="mapInfo">
			<h1>McGregor House</h1>
			<p>Former home of George McGregor, who with the help of Norman Rockwell, created the famous line-drawing of the Inn.</p>
			<a class="bookIt" href="https://booking.ihotelier.com/istay/istay.jsp?HotelID=17402">Book It</a>
			<a class="learnMore" href="<?php bloginfo('url'); ?>/berkshire-accommodations/guest-houses/mcgregor-house/">Learn More</a>
		</div>
	</div>

	<div class="mapAlt2 popF map_MapleGlen">
		<div class="mapInfo">
			<h1>Maple Glen</h1>
			<p>On the corner of South and Maple St. 17 rooms blending contemporary and vintage aesthetics with the traditional comforts of a New England home.</p>
			<a class="bookIt" href="https://booking.ihotelier.com/istay/istay.jsp?HotelID=17402">Book It</a>
			<a class="learnMore" href="<?php bloginfo('url'); ?>/berkshire-accommodations/guest-houses/maple-glen/">Learn More</a>
		</div>
	</div>

	<div class="mapAlt popD map_Stafford">
			<a name="down"></a>
		<div class="mapInfo">
			<h1>Stafford House</h1>
			<p>The Stafford House is a lovely former village residence formerly owned by Asa B Stafford, built in approximately 1850.</p>
			<a class="bookIt" href="https://booking.ihotelier.com/istay/istay.jsp?HotelID=17402">Book It</a>
			<a class="learnMore" href="<?php bloginfo('url'); ?>/berkshire-accommodations/guest-houses/stafford-house/">Learn More</a>
		</div>
	</div>

	<div class="mapAlt popC map_Stevens">
		<div class="mapInfo">
			<h1>Stevens House</h1>
			<p>Located on Pine Street, the Stevens House offers nine rooms, with a common living room, kitchen and dining area. </p>
			<a class="bookIt" href="https://booking.ihotelier.com/istay/istay.jsp?HotelID=17402">Book It</a>
			<a class="learnMore" href="<?php bloginfo('url'); ?>/berkshire-accommodations/guest-houses/stevens-house/">Learn More</a>
		</div>
	</div>

	<div class="mapPoint popA map_Meadowlark">
		<div class="mapInfo">
			<h1>Meadowlark</h1>
			<p>Located a scenic three miles from the Inn, this charming, rustic bungalow was once the "Little Studio" to sculptor Daniel Chester French.</p>
			<a class="bookIt" href="https://booking.ihotelier.com/istay/istay.jsp?HotelID=17402">Book It</a>
			<a class="learnMore" href="<?php bloginfo('url'); ?>/berkshire-accommodations/guest-houses/meadowlark/">Learn More</a>
		</div>
	</div>


</div>

<?php include('includes/menuBoards.php'); ?>


<div id="interiorMiddleWrap">
<div id="interiorMiddle">

<a class="directionsLink" href="<?php bloginfo('url');?>/property-map/directions-to-the-red-lion-inn/">Driving Directions</a>

</div><!-- /interiorMiddle -->
</div>

</div><!-- /interiorWrap-->

<?php get_footer(); ?>
