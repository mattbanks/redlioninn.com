
<?php get_header(); ?>

    <body id="home">
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

<div id="homeSlider">
<?php
		$slug = the_slug();
		$args = array(
			'posts_per_page' => 10,
			'post_type' => 'slide',
			'slide-page' => 'home-page'
		);
		query_posts($args);
		if ( have_posts() ) : ?>

		<div class="flexslider">
  			<ul class="slides">

		<?php while ( have_posts() ) : the_post(); ?>

    <li style="background-image: url('<?php the_field('image'); ?>'); background-position: center center; height: 1159px; ">
    	&nbsp;
    </li>

<?php endwhile; ?>

  </ul></div>

<?php endif; ?>
<?php wp_reset_query(); ?>
</div>

        <div id="topWrap">
        	<div id="topWrapBorder">
        	</div>
        </div>

     	<div id="bottomWrap">
     		<div id="bottomWrapBorder">
     		</div>
     	</div>

        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<?php if (get_field('home_image')) { ?>
	<div class="homeImg" style="background-image: url('<?php the_field('home_image'); ?>');">
		<?php if(get_field('home_image_url')) { ?><a href="<?php the_field('home_image_url'); ?>"></a><?php } ?>
	</div>
<?php } ?>

<?php get_footer(); ?>

