<?php
    ob_start();	
    session_start();
/*
Template Name: Lion's Den (Menu)
*/

?>

<?php
    require_once("menu-queries.php");
?>

<?php include('wp-load.php'); ?>
        	
<?php get_header(); ?>

    <body id="lionsden" <?php body_class('denPage'); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

         <?php include('includes/masthead.php'); ?> 
     
        <div id="denWrap">


		<a href="<?php bloginfo('url'); ?>/lions-den/"><img class="denLogo" src="<?php bloginfo('stylesheet_directory');?>/images/den/lionsDenLogo.png"></a>
		


<ul id="denNav">
<li><a class="calendar" href="<?php bloginfo('url'); ?>/events-calendar/category/lions-den/">Entertainment Calendar</a></li>
<li><a class="menu-bar" href="<?php bloginfo('url'); ?>/den/lions-den-menu/?mealID=126">Lion's Den Menu & Bar</a></li>
<li><a class="profiles" href="<?php bloginfo('url'); ?>/lions-den/artist-profiles/">Artist Profiles</a></li>
</ul>

</div>

<!-- BODY COPY GOES HERE -->
<div id="denBody">
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/den/menuHdr-den-v2.png">
<div>
	<ul id="dineMenuLinks">
            <?php for($x=0; $x<count($mealIDArray); $x++) { ?>
		<li><a href="?mealID=<?php echo $mealNameArray[$x]->term_id; ?>"><?php echo $mealNameArray[$x]->name; ?></a></li>
            <?php } ?>
		<li><a href="?mealID=winelist">Wine List</a></li>
	</ul>
</div>

<?php if(isset($_SESSION['mealID'])) { 
    if($_SESSION['mealID']!="") {
        if($_SESSION['mealID']=="winelist") { 
?>
<?php
            $page_id = 2127; 
            $page_data = get_page( $page_id );
            echo '<h1>'. $page_data->post_title .'</h1>';
            echo '<div class="menuItem">'. apply_filters('the_content', $page_data->post_content) .'</div>';
        } else { ?>
    <h1><?php echo $_SESSION['mealName']; ?><?php if(!stristr($_SESSION['mealName'], "menu")) { echo "&nbsp;Menu"; } ?></h1>
<?php for($x=0; $x<count($courseIDArray); $x++) {?>
            <h3 class="menuCategory"><?php echo $courseNameArray[$x]->name; ?></h3>
                <?php for($y=0; $y<count($courseMenuItemArray[$x]['menuItemIDArray']); $y++) { ?>
                <div class="menuItem">
                        <?php echo $courseMenuItemArray[$x]['menuItemArray'][$y]->post_title; ?>
                        <span class="menuPrice">$<?php echo money_format('%i', $courseMenuItemArray[$x]['menuItemInfoArray'][$y]['price1']); ?> <?php echo $courseMenuItemArray[$x]['menuItemInfoArray'][$y]['price1_label']; ?><?php if($courseMenuItemArray[$x]['menuItemInfoArray'][$y]['price2']!="" && $courseMenuItemArray[$x]['menuItemInfoArray'][$y]['price2']!==0 && $courseMenuItemArray[$x]['menuItemInfoArray'][$y]['price2']!="0") { echo " / $" . money_format('%i', $courseMenuItemArray[$x]['menuItemInfoArray'][$y]['price2']) . " " . $courseMenuItemArray[$x]['menuItemInfoArray'][$y]['price2_label']; } ?></span> <!-- this is price 1 & description, price 2 & description -->
                        <span class="menuDescription"><?php echo $courseMenuItemArray[$x]['menuItemInfoArray'][$y]['description']; ?></span>
                </div>
                <?php } ?>
            <div class="menuCategoryDesc"><?php echo $courseIDArray[$x]->description; ?></div>
    <?php } } } } ?>


<?php/*
$menuTerm = get_term_by('slug','breakfast','menu-category'); // This here just to illustrate

  $terms = get_cross_referenced_terms(array(
    'post_type'        => 'menu-category',
    'related_taxonomy' => 'breakfast',
    'term_id'          => $menuTerm->term_id,
  ));
  foreach($terms as $term) {
    echo "<p>{$term->name}</p>";
  }


//for each category, show posts
$cat_args=array(
  'orderby' => 'name',
  'order' => 'ASC',
  'taxonomy' => 'menu-category'
   );
   
$categories=get_categories($cat_args);
  foreach($categories as $category) {
    $args=array(
      'orderby' => 'title',
      'order' => 'ASC',
      'showposts' => -1,
      'category__in' => array($category->term_id),
      'caller_get_posts'=>1
    );
    $posts=get_posts($args);
      if ($posts) {
        echo '<p>Category: <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </p> ';
        foreach($posts as $post) {
          setup_postdata($post); ?>
          <p><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>
          <?php
        } // foreach($posts
      } // if ($posts
    } // foreach($categories
*/?>





</div><!-- /denBody -->


<?php include('includes/denfooter.php'); ?>