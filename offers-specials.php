<?php
/*
Template Name: Special Offers (Lodging Specials)
*/
?>
        	
<?php get_header(); ?>

    <body id="offers" <?php body_class($page_slug); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        
        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<div class="offersHeader">
<div class="lodgingTitle active">Lodging Specials</div>
<div class="packagesTitle"><a href="<?php bloginfo('siteurl'); ?>/travel-accommodations-packages/">Packages</a></div>
</div>

<div id="offersMiddle">
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<div class="calloutTitle"><?php the_title(); ?></div>
<div class="subTitle"><?php the_content(); ?></div>
<?php endwhile; endif; ?>
</div>


<div id="packagesMiddle">

<?php
$today = date('Ymd', strtotime('-1 day'));
$args = array(
	'meta_key' => 'expiration',
	'meta_value' => $today,
	'meta_compare' => '>',
	'posts_per_page' => 20,
	'post_type' => 'offers'
);
query_posts($args);
if ( have_posts() ) : while ( have_posts() ) : the_post();
?>

<div class="package">
	<div class="packageTitle"><?php the_field('room_house'); ?></div>
		<div class="packageSubtitle"><?php if(get_field('room_type')) {?>(<?php the_field('room_type');?>) <?php }?><?php the_field('offer_dates'); ?></div>

<?php if(get_field('image')) { ?>
	<div class="packageDetailsBlock">
		<img src="<?php the_field('image'); ?>">
	</div>
<?php } else { ?>
	<div class="packageDetailsBlock">
	</div>
<?php } ?>

	<div class="packagePriceBlock">
			<?php if(get_field('price')) { ?>

					<div class="packagePrice"><span>
				<?php echo '$' . get_field('price');
				} ?>
				<?php if(get_field('percentage')) {
				echo '' . get_field('percentage') . '%';
				} ?>
				
				
		</span>
		<span class="package-price-crossout">
					<?php if(get_field('price_1_original')) {
						echo '$' . get_field('price_1_original');
					} ?>
				</span>
		
		<?php the_field('eligible_days_of_week'); ?></div>
		


		<?php if(get_field('price_2')) { ?>

					<div class="packagePrice"><span>
				<?php echo '$' . get_field('price_2');
				} ?>
				<?php if(get_field('percentage_2')) {
				echo '' . get_field('percentage_2') . '%';
				} ?>
				
				
		</span>
		<span class="package-price-crossout">
					<?php if(get_field('price_2_original')) {
						echo '$' . get_field('price_2_original');
					} ?>
				</span>
		
		<?php the_field('eligible_days_of_week_2'); ?></div>


		<?php if(get_field('price_3')) { ?>

					<div class="packagePrice"><span>
				<?php echo '$' . get_field('price_3');
				} ?>
				<?php if(get_field('percentage_3')) {
				echo '' . get_field('percentage_3') . '%';
				} ?>
				
				
		</span>
		<span class="package-price-crossout">
					<?php if(get_field('price_3_original')) {
						echo '$' . get_field('price_3_original');
					} ?>
				</span>
		
		<?php the_field('eligible_days_of_week_3'); ?></div>

	
	

	<?php if(get_field('ihotelier_link')) { ?><a class="checkOffer" target="_blank" href="<?php the_field('ihotelier_link'); ?>">Check Rates and Availability</a><?php } else { ?> <div class="reserveText">To reserve, please call 413.298.1690 or email <a href="mailto:reservations@redlioninn.com">reservations@redlioninn.com</a></div> <?php } ?>
	<div class="packageSmall">Blackout dates may apply.<br/> <?php the_field('exclusions'); ?></div>
</div>

<?php endwhile; endif; ?>


	
</div><!-- /offersMiddle -->

</div><!-- /interiorWrap-->

<?php get_footer(); ?>