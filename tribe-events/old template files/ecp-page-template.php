<?php
/**
*  If 'Default Events Template' is selected in Settings -> The Events Calendar -> Theme Settings -> Events Template,
*  then this file loads the page template for all ECP views except for the individual
*  event view.  Generally, this setting should only be used if you want to manually
*  specify all the shell HTML of your ECP pages in this template file.  Use one of the other Theme
*  Settings -> Events Template to automatically integrate views into your
*  theme.
*
* You can customize this view by putting a replacement file of the same name (ecp-page-template.php) in the events/ directory of your theme.
*/

// Don't load directly
if ( !defined('ABSPATH') ) { die('-1'); }

?>
<?php get_header(); ?>

<?php
$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
if (false !== strpos($url,'events-calendar/category/lions-den')) { ?>

    <body id="lionsden" <?php body_class('denPage'); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

				<div id="masthead">
					<a class="homeBanner" href="<?php bloginfo('siteurl'); ?>">The Red Lion Inn - Home</a>
				</div>

        <div id="denWrap">


		<a href="<?php bloginfo('url'); ?>/?p=24"><img class="denLogo" src="<?php bloginfo('stylesheet_directory');?>/images/den/lionsDenLogo.png"></a>



<ul id="denNav">
<li><a class="calendar" href="<?php bloginfo('url'); ?>/events-calendar/category/the-lions-den/">Entertainment Calendar</a></li>
<li><a class="menu-bar" href="<?php bloginfo('url'); ?>/lions-den/lions-den-menu/">Lion's Den Menu & Bar</a></li>
<li><a class="profiles" href="<?php bloginfo('url'); ?>/?p=508">Artist Profiles</a></li>
</ul>

</div>

<!-- BODY COPY GOES HERE -->
<div id="denBody">

<?php tribe_events_before_html(); ?>
<?php include(tribe_get_current_template()); ?>
<?php tribe_events_after_html(); ?>

</div><!-- /denBody -->


<div id="denBottom">

	<div id="social">
		<ul>
        			<li><a class="fb" href="http://www.facebook.com/TheRedLionInnBerkshiresMA">Facebook</a></li>
        			<li><a class="twitter" href="http://twitter.com/TheRedLionInn">Twitter</a></li>
        			<li><a class="tripadvisor" href="http://www.tripadvisor.com/Hotel_Review-g41850-d89873-Reviews-The_Red_Lion_Inn-Stockbridge_Massachusetts.html">TripAdvisor</a></li>
        			<li><a class="blog" href="<?php bloginfo('url'); ?>/blog">Blog</a></li>
		</ul>
	</div><!-- /social -->


	<img class="denHours" src="<?php bloginfo('stylesheet_directory');?>/images/den/entertainmentHours.png" />


<!--	<a class="learnHistory" href="">Learn the History of the Lion's Den</a> -->


	<img class="denFooter" src="<?php bloginfo('stylesheet_directory');?>/images/den/denFooter.png" />

</div>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
        <?php wp_footer() ?>
    </body>
</html>

<?php } else { ?>

   <body id="default" <?php body_class($page_slug); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <div id="mainWrap">

       	<div id="masthead">
        		<ul>
        			<li class="nav1"><a href="<?php bloginfo('siteurl'); ?>/?p=4">Come & Stay</a></li>
        			<li class="nav2"><a href="<?php bloginfo('siteurl'); ?>/?p=12">Wine & Dine</a></li>
        			<li class="nav3"><a href="<?php bloginfo('siteurl'); ?>/?p=16">Meet & Celebrate</a></li>
        			<li class="nav4"><a href="<?php bloginfo('siteurl'); ?>/?p=182">Special Offers</a></li>
        			<li class="nav5"><a href="<?php bloginfo('siteurl'); ?>/?p=24">Lion's Den</a></li>
        			<li class="nav6"><a href="<?php bloginfo('siteurl'); ?>/gift-shop">The Gift Shop</a></li>
        		</ul>

        		<a class="homeBanner" href="<?php bloginfo('siteurl'); ?>">The Red Lion Inn - Home</a>
        		<a class="bookRoom" href="#">BOOK A ROOM</a>
        		<a class="phoneNumber" href="tel:4132985545">413-298-5545</a>

        	</div>

<div id="calendarWrap">

<?php tribe_events_before_html(); ?>
<h1 class="tribe-events-cal-title"><?php tribe_events_title(); ?></h1>
<?php include(tribe_get_current_template()); ?>
<?php tribe_events_after_html(); ?>

<?php get_footer(); ?>

<?php } ?>



