<?php
/**
*  If 'Default Events Template' is selected in Settings -> The Events Calendar -> Theme Settings -> Events Template, 
*  then this file loads the page template for all for the individual 
*  event view.  Generally, this setting should only be used if you want to manually 
*  specify all the shell HTML of your ECP pages in this template file.  Use one of the other Theme 
*  Settings -> Events Template to automatically integrate views into your 
*  theme.
*
* You can customize this view by putting a replacement file of the same name (ecp-single-template.php) in the events/ directory of your theme.
*/

// Don't load directly
if ( !defined('ABSPATH') ) { die('-1'); }
?>
<?php get_header(); ?>

    <body id="default" <?php body_class('events'); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        
        <div id="mainWrap">

       	<div id="masthead">
        		<ul>
        			<li class="nav1"><a href="<?php bloginfo('siteurl'); ?>/?p=4">Come & Stay</a></li>
        			<li class="nav2"><a href="<?php bloginfo('siteurl'); ?>/?p=12">Wine & Dine</a></li>
        			<li class="nav3"><a href="<?php bloginfo('siteurl'); ?>/?p=16">Meet & Celebrate</a></li>
        			<li class="nav4"><a href="<?php bloginfo('siteurl'); ?>/?p=182">Special Offers</a></li>
        			<li class="nav5"><a href="<?php bloginfo('siteurl'); ?>/?p=24">Lion's Den</a></li>
        			<li class="nav6"><a href="<?php bloginfo('siteurl'); ?>/gift-shop-coming-soon">The Gift Shop</a></li>
        		</ul>
        	
        		<a class="homeBanner" href="<?php bloginfo('siteurl'); ?>">The Red Lion Inn - Home</a>
        		<a class="bookRoom" href="https://booking.ihotelier.com/istay/istay.jsp?HotelID=17402" target="_blank">BOOK A ROOM</a>
        		<a class="phoneNumber" href="tel:4132985545">413-298-5545</a>
        		
        	</div>

<div id="interiorWrap">

<div id="interiorMiddleWrap">
<div id="interiorMiddle">


<?php tribe_events_before_html(); ?>
<div id="container">
	<div id="content" class="tribe-events-event widecolumn">
		<?php the_post(); global $post; ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php include(tribe_get_current_template()); ?>
			<?php edit_post_link(__('Edit','tribe-events-calendar'), '<span class="edit-link">', '</span>'); ?>
		</div><!-- post -->
		<?php if(tribe_get_option('showComments','no') == 'yes'){ comments_template(); } ?>
	</div><!-- #content -->
</div><!--#container-->
<?php tribe_events_after_html(); ?>



</div><!-- /interiorMiddle -->
</div>

</div><!-- /interiorWrap-->

<?php get_footer(); ?>
