<?php
/*
Template Name: Lion's Den
*/
?>
        	
<?php get_header(); ?>

    <body id="lionsden" <?php body_class('denPage'); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

         <?php include('includes/masthead.php'); ?> 
     
        <div id="denWrap">


		<a href="<?php bloginfo('url'); ?>/lions-den/"><img class="denLogo" src="<?php bloginfo('stylesheet_directory');?>/images/den/lionsDenLogo.png"></a>
		


<ul id="denNav">
<li><a class="calendar" href="<?php bloginfo('url'); ?>/events-calendar/category/lions-den/">Entertainment Calendar</a></li>
<li><a class="menu-bar" href="<?php bloginfo('url'); ?>/lions-den/lions-den-menu/?mealID=126">Lion's Den Menu & Bar</a></li>
<li><a class="profiles" href="<?php bloginfo('url'); ?>/lions-den/artist-profiles/">Artist Profiles</a></li>
</ul>

</div>

<!-- BODY COPY GOES HERE -->
<div id="denBody">

	<?php if (is_page('508')) { ?> <!-- ARTIST PROFILES -->
		<h1><span>the Lion's Den</span> Artist Profiles</h1>
		<p class="header"><?php 
$page_id = 508;
$page_data = get_page( $page_id );
echo $page_data->post_content;
?></p>
		
		<?php $args = array('posts_per_page' => 30,'post_type' => 'artist');
		query_posts($args);
		if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="artistProfile">
				<?php if ( get_field('picture') ) { ?><img src="<?php the_field('picture'); ?>" /><?php } ?>
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
				<?php if (get_field('website')) { ?><div class="webLink">Visit Website: <a href="<?php the_field('website'); ?>"><?php the_field('website'); ?></a></div><?php } ?>
			</div>
		<?php endwhile; endif; ?>

	<?php } else if (is_page('537')) { ?> <!-- Entertainment Calendar -->
		
		<?php include('events/gridview.php'); ?>
		<?php tribe_calendar_grid('28'); ?>
		
	<?php } else { ?>

		<?php if (have_posts()) : while (have_posts()) : the_post();?>
		<h1 class="balance-text"><?php the_title(); ?></h1>
		<?php the_content(); ?>
		<?php endwhile; endif; ?>

<?php } ?>
</div><!-- /denBody -->


<?php include('includes/denfooter.php'); ?>