<?php
/*
Template Name: Special Offers (Landing)
*/
?>
        	
<?php get_header(); ?>

    <body id="offers" <?php body_class($page_slug); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        
        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="interiorWrap">

<div class="offersHeader">
<div class="lodgingTitle active">Lodging Specials</div>
<div class="packagesTitle"><a href="<?php bloginfo('siteurl'); ?>/travel-accommodations-packages/">Packages</a></div>
</div>


<?php include('includes/menuBoards.php'); ?>

<div id="offersMiddle">

	<div class="calloutTitle">Affordable Berkshire Getaways!</div>
	<div class="subTitle">Looking for a steal? Here’s a list you won’t be able to resist!</div>


<?php
$today = date('Ymd', strtotime('-1 day'));
$args = array(
	'meta_key' => 'expiration',
	'meta_value' => $today,
	'meta_compare' => '>',
	'posts_per_page' => 20,
	'post_type' => 'offers'
);
query_posts($args);
if ( have_posts() ) : while ( have_posts() ) : the_post();

?>

<div class="offer">
	<div class="offerPrice">
			<?php if(get_field('price')) {
			echo '<span>$</span>' . get_field('price') . '';
			} ?>
			<?php if(get_field('percentage')) {
			echo '' . get_field('percentage') . '<span>%</span>';
			} ?>
	</div>
		<div class="offerImg"><img src="<?php the_field('image'); ?>"></div>
		<div class="offerDetails">
			<div class="offerRoomType">
			<?php the_field('room_type'); ?>
			</div>	
			<div class="offerRoom">
				<?php the_field('room_house'); ?>
			</div>
			<div class="offerDates"><?php the_field('offer_dates'); ?></div>
			<div class="offerDays"><?php the_field('eligible_days_of_week'); ?></div>
			<?php if(get_field('exclusions'))
						{
							echo '<span class="offerExcl">(' . get_field('exclusions') . ')</span>';
						}
				?>
			<?php if(get_field('ihotelier_link')) { ?><a class="checkOffer" target="_blank" href="<?php the_field('ihotelier_link'); ?>">Check Rates and Availability</a><?php } else { ?> <div class="reserveText">To reserve, please call 413.298.1690 or email <a href="mailto:reservations@redlioninn.com">reservations@redlioninn.com</a></div> <?php } ?>
		</div>
</div>

<?php endwhile; endif; ?>


	
</div><!-- /offersMiddle -->

</div><!-- /interiorWrap-->

<?php get_footer(); ?>