<?php
/*
Template Name: Sustainability & Community
*/
?>
        	
<?php get_header(); ?>

    <body id="community" <?php body_class('community'); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        
        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<div id="topNavText">
	<ul>
		<li><a href="<?php bloginfo('siteurl'); ?>/sustainability-community/local-farms">LOCAL PURVEYORS</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/sustainability-community/preservation">PRESERVATION/GREEN INITIATIVES</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/sustainability-community/community-engagement">COMMUNITY ENGAGEMENT</a></li>
	</ul>
</div>

<div id="interiorWrap">

<?php include('includes/sliders-headers.php'); ?>

<?php include('includes/menuBoards.php'); ?>


<div id="interiorMiddleWrap">
<div id="interiorMiddle">

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<h1 class="balance-text"><?php the_title(); ?></h1>
<?php the_content(); ?>
<?php endwhile; endif; ?>

</div><!-- /interiorMiddle -->
</div>

</div><!-- /interiorWrap-->

<?php get_footer(); ?>