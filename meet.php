<?php
/*
Template Name: Meet Section
*/
?>

<?php get_header(); ?>

    <body id="meet" <?php body_class($page_slug); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <div id="mainWrap">

<?php include('includes/masthead.php'); ?>

<?php include('includes/nav_Meet.php'); ?>

<div id="interiorWrap">

<?php include('includes/sliders-headers.php'); ?>

<div id="interiorMiddle">

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<h1 class="balance-text"><?php the_title(); ?></h1>
<?php the_content(); ?>
<?php endwhile; endif; ?>

</div><!-- /interiorMiddle -->

<div id="interiorLeft">

<?php include('includes/menuBoards.php'); ?>

</div>

<div id="interiorRight">

<?php if (is_tree(118)) { ?> <!-- meetings -->

<a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/meeting-spaces/rfp/" class="bookGroupEvent">Book a Group Event</a>

<ul class="tertiaryNav">
	<li><a href="<?php bloginfo('siteurl');?>/meetings-conferences/accommodations">Accommodations</a></li>
	<li><a href="<?php bloginfo('siteurl');?>/meetings-conferences/meeting-spaces/meals-breaks">Meals <span class="amp">&</span> Breaks</a></li>
	<li><a href="<?php bloginfo('siteurl');?>/meetings-conferences/meeting-spaces/teambuilding-activities">Teambuilding Activities</a></li>
	<li><a href="<?php bloginfo('siteurl');?>/meetings-conferences/meeting-spaces/equipment-av">Equipment / AV</a></li>
	<li><a href="<?php bloginfo('siteurl');?>/meetings-conferences/meeting-spaces/rfp">Request for Proposal</a></li>
	<li><a href="<?php bloginfo('siteurl');?>/meetings-conferences/meeting-spaces/corporate-negotiated-rates/">Corporate Negotiated Rates</a></li>
	<li><a href="<?php bloginfo('siteurl');?>/meetings-conferences/meeting-spaces/associations">Associations</a></li>
	<li><a href="<?php bloginfo('siteurl'); ?>/things-to-do-in-the-berkshires/">The Berkshires</a></li>
</ul>
<?php } else if (is_tree(112)) { ?> <!-- MEET & CELEBRATE: Weddings at the Red Lion Inn -->

<a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/weddings/rfp/" class="bookGroupEvent">Book a Group Event</a>

<ul class="tertiaryNav">
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/accommodations/">Accommodations</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/weddings/rehearsal-dinners">Rehearsal Dinners</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/weddings/the-ceremony">The Ceremony</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/weddings/the-reception">The Reception</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/weddings/resources">Resources</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/weddings/weddings-gallery">Photo Gallery</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/weddings/rfp">Request for Proposal</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/things-to-do-in-the-berkshires/">The Berkshires</a></li>
		</ul>
	</div>

<?php } else if (is_tree(114)) { ?> <!-- MEET & CELEBRATE: Family Gatherings -->

<a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/family-accommodations/catering-rfp/" class="bookGroupEvent">Book a Group Event</a>

<ul class="tertiaryNav">
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/accommodations/">Accommodations</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/family-accommodations/reunions">Reunions</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/family-accommodations/anniversaries">Anniversaries</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/family-accommodations/social-gatherings">Social Gatherings</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/family-accommodations/holiday-parties">Holiday Parties</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/berkshire-dining/private-dining/">Private Dining</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/family-accommodations/catering-rfp/">Request for Proposal</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/things-to-do-in-the-berkshires/">The Berkshires</a></li>
		</ul>
<?php } else if (is_tree(116)) { ?> <!-- MEET & CELEBRATE: Group Tours -->

<a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/berkshire-group-tours/group-leader-rfp/" class="bookGroupEvent">Book a Group Event</a>

<ul class="tertiaryNav">
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/accommodations/">Accommodations</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/berkshire-group-tours/dining/">Dining</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/berkshire-group-tours/itineraries-activities/">Itineraries <span class="amp">&</span> Activities</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/berkshire-group-tours/group-leader-rfp/">Group Leader RFP</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/meetings-conferences/berkshire-group-tours/professional-tour-operator-rfp/">Professional Tour Operator RFP</a></li>
		<li><a href="<?php bloginfo('siteurl'); ?>/things-to-do-in-the-berkshires/">The Berkshires</a></li>
		</ul>

<?php } ?>

</div><!-- /interiorWrap-->


<?php get_footer(); ?>
